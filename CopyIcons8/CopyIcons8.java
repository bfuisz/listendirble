import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

public class CopyIcons8 {

	public static void copyFile(File sourceFile, File destFile) throws IOException {
		if (!destFile.exists()) {
			destFile.createNewFile();
		}

		FileChannel source = null;
		FileChannel destination = null;

		try {
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();
			destination.transferFrom(source, 0, source.size());
		} finally {
			if (source != null) {
				source.close();
			}
			if (destination != null) {
				destination.close();
			}
		}

	}

	public static boolean isPng(String filename) {
		String extension = "";
		int i = filename.lastIndexOf('.');
		if (i > 0) {
			extension = filename.substring(i + 1);
		}
		return extension.equalsIgnoreCase("png");
	}

	public static void main(String[] args) throws IOException {
		if (args.length < 2) {
			System.err.println("Usage src dst (path to res folder with hdpi mdpi xhdpi and xxhdpi e.g.(app\\src\\main\\res))");
			return;
		}
		String sourcePath = args[0];
		String destinationPath = args[1];

		File source = new File(sourcePath);
		if (!source.exists() || !source.isDirectory()) {
			throw new IllegalArgumentException("Source has to be a directory");
		}
		File[] filesInSource = source.listFiles();
		List<File> filesNeeded = new ArrayList<>(filesInSource.length);

		String name = "";
		for (File tmp : filesInSource) {
			name = tmp.getName();
			// pretty scetchy but should work for icons8
			if (name.contains("-") && isPng(name)) {
				filesNeeded.add(tmp);
			}
		}
		if (filesNeeded.size() <= 3) {
			throw new RuntimeException("Not enough or no suitable files found format should be name-100.png");
		}
		File hdpi = new File(destinationPath, "drawable-hdpi");
		File mdpi = new File(destinationPath, "drawable-mdpi");
		File xhdpi = new File(destinationPath, "drawable-xhdpi");
		File xxhdpi = new File(destinationPath, "drawable-xxhdpi");

		for (File tmp : filesNeeded) {
			name = tmp.getName();

			if (name.contains("-100")) {
				name = name.replace("-100", "");
				copyFile(tmp, new File(xxhdpi, name));
			}
			if (name.contains("-64")) {
				name = name.replace("-64", "");
				copyFile(tmp, new File(xhdpi, name));
			}
			if (name.contains("-50")) {
				name = name.replace("-50", "");
				copyFile(tmp, new File(hdpi, name));
			}
			if (name.contains("-32")) {
				name = name.replace("-32", "");
				copyFile(tmp, new File(mdpi, name));
			}
		}
	}
}

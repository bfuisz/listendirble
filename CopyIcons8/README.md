Small tool to help copy [icons8](http://icons8.com/) icons to /res folder
they usually look like this name-100.png  
sample usage: 
java -jar copyIcons8.jar c:\Users\...\Downloads c:\Users\...\workspace\...\app\src\main\res
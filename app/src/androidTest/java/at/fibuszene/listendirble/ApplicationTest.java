package at.fibuszene.listendirble;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;


import api.StationsApi;
import model.RadioStation;
import retrofit.RestAdapter;
import utils.Constants;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }


    public void testGetStation() {
        StationsApi api = new RestAdapter.Builder().setEndpoint(Constants.ENDPOINT).build().create(StationsApi.class);
        RadioStation station = api.getStation(Constants.API_KEY, 7);
        Log.d("station", station + "");
    }
}
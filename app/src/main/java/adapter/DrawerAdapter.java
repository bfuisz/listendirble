package adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import at.fibuszene.listendirble.MainActivity;
import at.fibuszene.listendirble.R;
import model.RadioStation;

/**
 * Created by benedikt.
 */
public class DrawerAdapter extends BaseAdapter {

    private List<RadioStation> stations;
    private Context context;

    public DrawerAdapter(Context context, List<RadioStation> station) {
        this.stations = station;
        this.context = context;
    }

    public DrawerAdapter(Context context) {
        this.context = context;
        this.stations = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return stations.size();
    }

    @Override
    public RadioStation getItem(int position) {
        try {
            return stations.get(position);
        } catch (IndexOutOfBoundsException iob) {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        try {
            return getItem(position).getId();
        } catch (NullPointerException npe) {
            return -1;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final RadioStation station = getItem(position);
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.drawer_item, null);
        }
        if (station != null) {
            ((TextView) convertView.findViewById(R.id.favoriteItemTextView)).setText(station.getName());
            ImageView favorite = (ImageView) convertView.findViewById(R.id.favoriteButton);
            favorite.setActivated(true);
            favorite.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (context instanceof MainActivity) {
                        if (((MainActivity) context).unfavorite(station.getId())) {
                            stations.remove(station);
                            notifyDataSetChanged();
                        }
                    }
                }
            });
        }
        return convertView;
    }
}

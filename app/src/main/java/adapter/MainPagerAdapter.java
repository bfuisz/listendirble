package adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import fragment.CategoryFragment;
import fragment.RegionFragment;
import fragment.SearchFragment;

/**
 * Created by benedikt.
 */
public class MainPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> pages;


    public MainPagerAdapter(FragmentManager fm) {
        super(fm);
        this.pages = new ArrayList<>();
        pages.add(CategoryFragment.getInstance());
        pages.add(RegionFragment.getInstance());
        pages.add(SearchFragment.getInstance());
    }


    @Override
    public Fragment getItem(int position) {
        try {
            return pages.get(position);
        } catch (IndexOutOfBoundsException iobe) {
            return pages.get(0);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Categories";
            case 1:
                return "Countries";
            case 2:
                return "Search";
        }
        return super.getPageTitle(position);
    }

    @Override
    public int getCount() {
        return pages.size();
    }
}

package adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import at.fibuszene.listendirble.MainActivity;
import at.fibuszene.listendirble.R;
import at.fibuszene.listendirble.StationsActivity;
import model.Image;
import model.RadioStation;
import persistency.helper.RadioStationHelper;

/**
 * Created by benedikt.
 */
public class StationsArrayAdapter extends ArrayAdapter {
    private List<RadioStation> stationList;

    public StationsArrayAdapter(Context context) {
        super(context, R.layout.station_preview_item);
        stationList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return stationList.size();
    }

    @Override
    public RadioStation getItem(int position) {
        try {
            return stationList.get(position);
        } catch (IndexOutOfBoundsException iob) {
            return null;
        }
    }


    @Override
    public long getItemId(int position) {
        try {
            return getItem(position).getId();
        } catch (NullPointerException npe) {
            return -1;
        }
    }

    //cool waer die liste in einen cursor wrappen und stationscursoradapter verwenden
    //und wieder waers cool alle kategorien zu haben
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RadioStation station = getItem(position);
        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.station_preview_item, null);
        }
        TextView txtView = (TextView) convertView.findViewById(R.id.stationName);
        txtView.setText(station.getName());
        View view = convertView.findViewById(R.id.statusView);
        view.setActivated(station.getStatus() == 0);

        ImageView imageButton = (ImageView) convertView.findViewById(R.id.browserButton);
        final String url = station.getWebsite();
        if (url != null && !TextUtils.isEmpty(url)) {
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    getContext().startActivity(intent);
                }
            });
            imageButton.setVisibility(View.VISIBLE);//if reuse and previouse was View.GONE
        } else {
            imageButton.setVisibility(View.GONE);
        }

        imageButton = (ImageView) convertView.findViewById(R.id.countryButton);
        final int id = getContext().getResources().getIdentifier(station.getCountry().toLowerCase(), "drawable", getContext().getPackageName());
        try {
            imageButton.setImageDrawable(getContext().getResources().getDrawable(id));
        } catch (RuntimeException rte) {
            //TODO: FrameLayoutOverlay with TextView
        }

        return convertView;
    }

    public void setList(List<RadioStation> list) {
        this.stationList = list;
    }
}

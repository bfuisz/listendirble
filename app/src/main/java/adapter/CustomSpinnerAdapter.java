package adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import at.fibuszene.listendirble.BaseActivity;
import at.fibuszene.listendirble.R;
import model.Category;

/**
 * Created by benedikt.
 */
public class CustomSpinnerAdapter extends BaseAdapter {
    private Context context;
    private List<Category> childCategories;

    public CustomSpinnerAdapter(Context context, List<Category> childCategories) {
        this.context = context;
        if (childCategories == null) {
            childCategories = new ArrayList<>();
        }
        childCategories.add(0, new Category());
        this.childCategories = childCategories;
    }

    public CustomSpinnerAdapter(Context context) {
        this.context = context;
        this.childCategories = new ArrayList<>();
        childCategories.add(0, new Category());
    }

    @Override
    public int getCount() {
        return this.childCategories.size();
    }

    @Override
    public Category getItem(int position) {
        try {
            return childCategories.get(position);
        } catch (IndexOutOfBoundsException iob) {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        try {
            return getItem(position).getId();
        } catch (NullPointerException npe) {
            return -1;
        }
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.textview, null);
        }
        ((TextView) convertView.findViewById(R.id.customTextView)).setText(getItem(position).getName());
        return convertView;
    }

    public void setList(List<Category> list) {
        this.childCategories = list;
    }
}

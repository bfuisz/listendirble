package adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import at.fibuszene.listendirble.MainActivity;
import at.fibuszene.listendirble.R;
import model.Image;

/**
 * Created by benedikt.
 */
public class RegionAdapter extends ArrayAdapter {
    private TypedArray regions;
    private TypedArray icons;

    public RegionAdapter(Context context) {
        super(context, R.layout.region_item);
        regions = context.getResources().obtainTypedArray(R.array.regions);
        icons = context.getResources().obtainTypedArray(R.array.drawable_regions);

    }

    @Override
    public int getCount() {
        return regions.length();
    }

    @Override
    public String getItem(int position) {
        try {
            return regions.getString(position);
        } catch (IndexOutOfBoundsException iob) {
            return null;
        }
    }

    public Drawable getDrawable(int position) {
        try {
            return icons.getDrawable(position);
        } catch (IndexOutOfBoundsException iob) {
            return null;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.region_item, null);
        }
        TextView txtView = (TextView) convertView.findViewById(R.id.regionName);
        txtView.setTypeface(MainActivity.HEADING_FONT);
        txtView.setText(getItem(position));

        ((ImageView) convertView.findViewById(R.id.regionPic)).setImageDrawable(getDrawable(position));
        return convertView;
    }
}

package adapter;

import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.support.v4.widget.ResourceCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import at.fibuszene.listendirble.R;
import model.Category;

/**
 * Created by benedikt.
 */
public class GridViewAdapter extends ResourceCursorAdapter {


    public GridViewAdapter(Context context, int layout) {
        super(context, layout, null, false);
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        Category cat = Category.from(cursor);
        ((TextView) view.findViewById(R.id.categoryName)).setText(cat.getName());
        ((TextView) view.findViewById(R.id.categoryDescription)).setText(cat.getDescription());
    }

    @Override
    public Category getItem(int position) {
        getCursor().moveToPosition(position);
        return Category.from(getCursor());
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }
}

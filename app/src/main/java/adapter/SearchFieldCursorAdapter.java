package adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

/**
 * Created by benedikt.
 */
public class SearchFieldCursorAdapter extends CursorAdapter {

    public SearchFieldCursorAdapter(Context context, Cursor c) {
        super(context, c, false);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

    }
}

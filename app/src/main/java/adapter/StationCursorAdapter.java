package adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Browser;
import android.support.v4.widget.ResourceCursorAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import at.fibuszene.listendirble.R;
import at.fibuszene.listendirble.StationsActivity;
import model.Category;
import model.RadioStation;
import persistency.helper.RadioStationHelper;

/**
 * Created by benedikt.
 */
public class StationCursorAdapter extends ResourceCursorAdapter {


    public StationCursorAdapter(Context context, int layout) {
        super(context, layout, null, false);
    }


    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final RadioStation station = RadioStation.from(cursor);
        ((TextView) view.findViewById(R.id.stationName)).setText(station.getName());
        ImageView imageView = (ImageView) view.findViewById(R.id.favoriteButton);
        imageView.setActivated(station.isFavorite());

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean state = station.toggleFavorite();
                v.setActivated(state);
                RadioStationHelper.saveOrUpdate(context, station);
            }
        });

        imageView = (ImageView) view.findViewById(R.id.browserButton);
        final String url = station.getWebsite();
        if (url != null && !TextUtils.isEmpty(url)) {
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    context.startActivity(intent);
                }
            });
            imageView.setVisibility(View.VISIBLE);//if reuse and previouse was View.GONE
        } else {
            imageView.setVisibility(View.GONE);
        }
        imageView = (ImageView) view.findViewById(R.id.countryButton);

        final int id = context.getResources().getIdentifier(station.getCountry().toLowerCase(), "drawable", context.getPackageName());
        try {
            imageView.setImageDrawable(context.getResources().getDrawable(id));
        } catch (RuntimeException rte) {
            //TODO: FrameLayoutOverlay with TextView
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof StationsActivity) {
                    ((StationsActivity) context).sortBy(station.getCountry(), id);
                }
            }
        });

        imageView = (ImageView) view.findViewById(R.id.stationImage);
        try {
            //bissl eigenartig auf die route stations/apikey/api_key/id/63/format/json
            //bekommt man die bilder nicht mitgeliefert
            //d.h. koennen nur gespeichert werden wenn der user schon einmal bei der station war
            //ausser man ladet immer alle infos zur station was unnoetig waer und ineffizient weil die ganze songhistory dabei waer.
            // vl dirbel a mail schicken...
            Picasso.with(context).load(station.getImage()).into(imageView);
        } catch (IllegalArgumentException ilArg) {
            ilArg.printStackTrace();
            //station does not provide a picuter
            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_launcher));
        }

        View layout = (View) view.findViewById(R.id.statusView);
        layout.setActivated(station.getStatus() == 0); //eigenartig 0 is down up = activated false
    }

    @Override
    public RadioStation getItem(int position) {
        try {
            getCursor().moveToPosition(position);
            return RadioStation.from(getCursor());
        } catch (NullPointerException npe) {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        try {
            return getItem(position).getId();
        } catch (NullPointerException npe) {
            return -1;
        }
    }
}

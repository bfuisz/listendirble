package adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import at.fibuszene.listendirble.R;
import model.SongHistoryItem;

/**
 * Created by benedikt.
 */
public class SongHistoryAdapter extends BaseAdapter {
    private Context context;
    private List<SongHistoryItem> list;

    public SongHistoryAdapter(Context context, List<SongHistoryItem> list) {
        this.context = context;
        this.list = list;
    }

    public SongHistoryAdapter(Context context) {
        this.context = context;
        this.list = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public SongHistoryItem getItem(int position) {
        try {
            return list.get(position);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        try {
            return getItem(position).hashCode();
        } catch (NullPointerException npe) {
            return -1;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SongHistoryItem item = getItem(position);
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.song_history_item, null);
        }

        ((TextView) convertView.findViewById(R.id.trackName)).setText(item.getTitle());
        ((TextView) convertView.findViewById(R.id.trackArtist)).setText(item.getName());
        ((TextView) convertView.findViewById(R.id.date)).setText(item.getDate() + "");
        Picasso.with(context).load(item.getThumbnail()).into(((ImageView) convertView.findViewById(R.id.thumbnail)));
        return convertView;
    }


    public void addAll(List<SongHistoryItem> historyItems) {
        if (this.list == null) {
            this.list = new ArrayList<>();
        }
        this.list.addAll(historyItems);
        notifyDataSetChanged();
    }

    public void add(List<SongHistoryItem> historyItems) {
        if (this.list == null) {
            this.list = new ArrayList<>();
        }
        //TODO: noch zu verbessern nicht die schoenste loesung
        //arbeitet unter der annahme, dass der user nit immer auf die liste schaut
        //schoener replace/add
        this.list.clear();
        this.list.addAll(historyItems);
        notifyDataSetChanged();
    }
}

package service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.audiofx.Equalizer;
import android.media.audiofx.Visualizer;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.net.Uri;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import at.fibuszene.listendirble.R;
import at.fibuszene.listendirble.SingleStationActivity;

public class PlaybackService extends Service {
    public enum MessageType {
        loading(1), started(2), error(3);
        private int value;

        private MessageType(int value) {
            this.value = value;
        }

        public static MessageType valueOf(int val) {
            switch (val) {
                case 1:
                    return loading;
                case 2:
                    return started;
                case 3:
                    return error;
            }
            return started;
        }

        public static int valueOf(MessageType type) {
            switch (type) {
                case loading:
                    return 1;
                case started:
                    return 2;
                case error:
                    return 3;
            }
            return 1;
        }


    }

    public static final String SERVICE_PLAY_ACTION = "at.fibuszene.listendirble.stream_url"; //url
    public static final String SERVICE_STOP_ACTION = "at.fibuszene.listendirble.stop_stream_url"; //url
    public static final String SERVICE_PAUSE_ACTION = "at.fibuszene.listendirble.stop_stream_url"; //url
    public static final String SERVICE_ARG1_URL = "at.fibuszene.listendirble.arg1"; //url
    public static final String SERVICE_ARG2_STATION_ID = "at.fibuszene.listendirble.arg2"; //station id
    public static final String SERVICE_ARG3_MESSENGER = "at.fibuszene.listendirble.arg3"; //messenger
    private Messenger messenger;
    private MediaPlayer mp;
    public static final int NOTIFICATION_ID = 100;
    private long stationId;
    private ExecutorService executorService;


    private NotificationManager mNotificationManager;


    public PlaybackService() {
        this.executorService = Executors.newCachedThreadPool();
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getAction() != null) {
            if (intent.getAction().equals(SERVICE_PLAY_ACTION)) {
                String newUrl = intent.getStringExtra(SERVICE_ARG1_URL);
                stationId = intent.getLongExtra(SERVICE_ARG2_STATION_ID, 0);
                messenger = intent.getParcelableExtra(SERVICE_ARG3_MESSENGER);
                if (mp != null) {
                    mp.start();
                } else {
                    Uri myUri = Uri.parse(newUrl);
                    final Message msg = new Message();
                    mp = new MediaPlayer();
                    final Equalizer eq = new Equalizer(0, mp.getAudioSessionId());


                    try {
                        mp.setDataSource(this, myUri);
                        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);

                        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                            @Override
                            public void onPrepared(MediaPlayer mp) {
                                mp.start();
                                msg.arg1 = MessageType.valueOf(MessageType.started);
                                msg.arg2 = mp.getAudioSessionId();
                                sendMessage(msg);
                                showNotification();

                            }
                        });
                        mp.prepareAsync();
                    } catch (IllegalArgumentException | IllegalStateException | SecurityException | IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (intent.getAction().equals(SERVICE_PAUSE_ACTION)) {
                if (mp != null) {
                    mp.pause();
                    destroyNotification();
                }
            }
            if (intent.getAction().equals(SERVICE_STOP_ACTION)) {
                if (mp != null) {
                    mp.stop();
                    mp.release();
                    mp = null;
                }
            }
        }
        return START_STICKY;
    }


    public void sendMessage(Message message) {
        if (messenger != null) {
            try {
                messenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
                //whelp f***
            }
        }
    }

    public void destroyNotification() {
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(NOTIFICATION_ID);
    }

    public void showNotification() {

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("My notification")
                        .setContentText("Hello World!");
        Intent resultIntent = new Intent(this, SingleStationActivity.class);
        resultIntent.putExtra(SingleStationActivity.STATION_ID, this.stationId);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(SingleStationActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        notificationBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.


        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.notification_view);

        setListeners(contentView);//look at step 3

        Notification notification = notificationBuilder.build();
        notification.contentView = contentView;
        mNotificationManager.notify(NOTIFICATION_ID, notification);

    }


    public void setListeners(RemoteViews view) {
        //Follows exactly my code!
        Intent volume = new Intent(this, PlaybackService.class);
        volume.setAction(SERVICE_PAUSE_ACTION);
        //HERE is the whole trick. Look at pVolume. I used 1 instead of 0.
        PendingIntent pVolume = PendingIntent.getService(this, 1, volume, 0);
        view.setOnClickPendingIntent(R.id.pause, pVolume);
    }
}


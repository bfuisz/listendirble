package service.tasks;

import android.app.LoaderManager;
import android.content.Context;
import android.util.Log;

import java.util.List;

import api.StationsApi;
import model.Category;
import model.RadioStation;
import persistency.contracts.CategoryContract;
import persistency.contracts.RadioStationContract;
import retrofit.RestAdapter;
import service.NetworkService;
import utils.Constants;

/**
 * Created by benedikt.
 */
public class RadioStationTask extends Task {
    private static final long serialVersionUID = -7745765416011813931L;

    public RadioStationTask() {
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public void run() {
        List<RadioStation> radioStations = null;
        RadioStation radioStation = null;
        StationsApi api = NetworkService.stationsApi;
        switch (taskType) {

            case listCategories:
            case listPrimaryCategories:
            case listChildCategories:
                Log.e("RadioStationTask", "Wrong Task");
                break;
            case listCategoryStations:
                radioStations = api.listCategoryStations(Constants.API_KEY, longExtra);
                for (RadioStation station : radioStations) {
                    station.setCategoryId(longExtra);
                    Log.d("Stations", station + " " + longExtra);

                }
                break;
            case listContinentStations:
                radioStations = api.listContinentStations(Constants.API_KEY, stringExtra);
                for (RadioStation station : radioStations) {
                    station.setRegion(stringExtra);
                }
                
                break;
            case listCountryStations:
                radioStations = api.listCountryStations(Constants.API_KEY, stringExtra);
                break;
            case getStation:
                radioStation = api.getStation(Constants.API_KEY, longExtra);

                break;
            //unused
            case getStationAmount:
                api.getStationAmount(Constants.API_KEY);
                break;
        }

        if (context != null) {
            if (radioStations != null) {
                StorageTask.store(context, RadioStationContract.RadioStationEntry.CONTENT_URI, radioStations);
            }
            if (radioStation != null) {
                StorageTask.store(context, RadioStationContract.RadioStationEntry.CONTENT_URI, radioStation);
            }
        }
    }


}

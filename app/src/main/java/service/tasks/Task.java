package service.tasks;

import android.content.Context;

import java.io.Serializable;

/**
 * Created by benedikt.
 */
public abstract class Task implements Runnable, Serializable {


    public enum TaskType {
        listCategories, listPrimaryCategories, listChildCategories,
        listCategoryStations, listContinentStations, listCountryStations, getStation, getStationAmount;
    }

    protected Context context;
    protected TaskType taskType;
    protected long longExtra;
    protected String stringExtra;

    /**
     * Do not call this one !!!
     * the whole object gets Serialized and if the context doesn't like that
     * e.g. Activity with ViewWidgets then the whole thing will crash.
     * The NetworkService will add the Context.
     *
     * @param context
     */
    public abstract void setContext(Context context);


    public Task setTaskType(TaskType taskType) {
        this.taskType = taskType;
        return this;
    }

    public Task setLongExtra(long longExtra) {
        this.longExtra = longExtra;
        return this;
    }

    public Task setStringExtra(String stringExtra) {
        this.stringExtra = stringExtra;
        return this;
    }


}

package service.tasks;

import android.content.Context;
import android.util.Log;

import java.util.List;

import api.CategoryApi;
import api.StationsApi;
import model.Category;
import persistency.contracts.CategoryContract;
import retrofit.RestAdapter;
import service.NetworkService;
import utils.Constants;

/**
 * Created by benedikt.
 */
public class CategoryNetworkTask extends Task {
    private static final long serialVersionUID = 1753767580481482648L;


    public CategoryNetworkTask() {
    }


    public void setContext(Context context) {
        this.context = context;
    }


    @Override
    public void run() {
        List<Category> categoryList = null;
        CategoryApi api = NetworkService.categoryApi;
        switch (taskType) {
            case listCategories:
                categoryList = api.listCategories(Constants.API_KEY);
                break;
            case listPrimaryCategories:
                categoryList = api.listPrimaryCategories(Constants.API_KEY);
                break;
            case listChildCategories:
                categoryList = api.listChildCategories(Constants.API_KEY, longExtra);

                for (Category category : categoryList) {
                    category.setParent(longExtra);
                }
                break;

            case listCategoryStations:
            case listContinentStations:
            case listCountryStations:
            case getStation:
            case getStationAmount:
                Log.e("CategoryNetworkTask", "Wrong Task");
                break;
        }

        if (context != null && categoryList != null) {
            StorageTask.store(context, CategoryContract.CategoryEntry.CONTENT_URI, categoryList);
        }
    }
}

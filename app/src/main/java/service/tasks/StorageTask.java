package service.tasks;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import java.util.List;

import model.Valuable;

/**
 * Created by benedikt.
 */
public class StorageTask {


    public static void store(Context context, Uri uri, List<? extends Valuable> list) {
        ContentValues[] values = new ContentValues[list.size()];

        int i = 0;
        for (Valuable valuable : list) {
            values[i] = valuable.values();
            i++;
        }
        context.getContentResolver().bulkInsert(uri, values);
    }

    public static void store(Context context, Uri uri, Valuable valuable) {
        if (valuable != null) {
            context.getContentResolver().insert(uri, valuable.values());
        }
    }

}

package service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import api.CategoryApi;
import api.StationsApi;
import retrofit.RestAdapter;
import service.tasks.Task;
import utils.Constants;

public class NetworkService extends Service {
    public static CategoryApi categoryApi;
    public static StationsApi stationsApi;


    public static final String SERVICE_ARG1 = "at.fibuszene.listendirble.task_description"; //url

    private ExecutorService executorService;

    public NetworkService() {
        executorService = Executors.newCachedThreadPool();
        categoryApi = new RestAdapter.Builder().setEndpoint(Constants.ENDPOINT).build().create(CategoryApi.class);
        stationsApi = new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(Constants.ENDPOINT).build().create(StationsApi.class);
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.hasExtra(SERVICE_ARG1)) {
            Task task = (Task) intent.getSerializableExtra(SERVICE_ARG1);

            if (task != null) {
                task.setContext(this);
                executorService.submit(task);
            }
        }
        return START_STICKY;
    }
}

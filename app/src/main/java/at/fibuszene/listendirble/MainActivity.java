package at.fibuszene.listendirble;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.astuetz.PagerSlidingTabStrip;

import adapter.DrawerAdapter;
import adapter.GridViewAdapter;
import adapter.MainPagerAdapter;
import model.RadioStation;
import persistency.contracts.CategoryContract;
import persistency.contracts.RadioStationContract;
import persistency.helper.RadioStationHelper;
import service.NetworkService;
import service.tasks.CategoryNetworkTask;
import service.tasks.Task;
import utils.Constants;


public class MainActivity extends BaseActivity {
    private DrawerLayout drawer;
    public static Typeface HEADING_FONT;

    private DrawerAdapter drawerAdapter;
    private MainPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarIcon(R.drawable.ic_drawer);
        Constants.API_KEY = getResources().getString(R.string.api_key);
        HEADING_FONT = Typeface.createFromAsset(getAssets(), "fonts/steelfish_outline.ttf");

        ViewPager mainPager = (ViewPager) findViewById(R.id.mainPager);
        pagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        mainPager.setAdapter(pagerAdapter);
        // Bind the tabs to the ViewPager
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(mainPager);


        drawer = (DrawerLayout) findViewById(R.id.drawer);
        drawer.setDrawerShadow(R.drawable.drawer_shadow, Gravity.START);

        ListView drawerList = (ListView) findViewById(R.id.drawerList);
        drawerAdapter = new DrawerAdapter(this, RadioStationHelper.list(this, RadioStationContract.RadioStationEntry.COLUMN_FAVORITE + " = 1", null));
        drawerList.setAdapter(drawerAdapter);
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SingleStationActivity.launch(MainActivity.this, id);
            }
        });

    }

    public boolean unfavorite(long stationId) {
        ContentValues value = new ContentValues();
        value.put(RadioStationContract.RadioStationEntry.COLUMN_FAVORITE, 0);
        return getContentResolver().update(RadioStationContract.RadioStationEntry.CONTENT_URI, value, RadioStationContract.RadioStationEntry._ID + " = ?", new String[]{stationId + ""}) > 0;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawer.openDrawer(Gravity.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

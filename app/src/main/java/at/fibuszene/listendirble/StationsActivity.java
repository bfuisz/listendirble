package at.fibuszene.listendirble;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import adapter.CustomSpinnerAdapter;
import adapter.StationCursorAdapter;
import model.Category;
import persistency.contracts.CategoryContract;
import persistency.contracts.RadioStationContract;
import persistency.helper.CategoryHelper;
import service.NetworkService;
import service.tasks.CategoryNetworkTask;
import service.tasks.RadioStationTask;
import service.tasks.Task;
import utils.Constants;


public class StationsActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemSelectedListener {
    private static final String CATEGORY_ID_EXTRA = "at.fibuszene.listendirble.category_id_extra";
    private static final String REGION_EXTRA = "at.fibuszene.listendirble.region_extra";
    private boolean regions;
    private long categoryId;
    private String region;
    private StationCursorAdapter stationCursorAdapter;
    private LinearLayout linearLayout;
    private TextView sortOrderView;
    private ImageView flagSortOrder;

    private String selection;
    private String[] selectionArgs;
    private String sortOrder;

    private CustomSpinnerAdapter adapter;
    private CatContentObserver catObserver;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        if (intent.hasExtra(CATEGORY_ID_EXTRA)) {
            this.categoryId = intent.getLongExtra(CATEGORY_ID_EXTRA, 0);
            regions = false;
            // sub categories nur wenn kategorien angezeit werden
            adapter = new CustomSpinnerAdapter(this, CategoryHelper.listChildCategories(this, categoryId));
            spinner = new Spinner(this);

            if (determineSpinnerVisibility()) {
                spinner.setAdapter(adapter);
                spinner.setOnItemSelectedListener(this);
                toolbar.addView(spinner);
            }

            //selection fuer kategorien
            setStandartSelection();
            catObserver = new CatContentObserver(new Handler());
            getContentResolver().registerContentObserver(CategoryContract.CategoryEntry.CONTENT_URI, true, catObserver);
            loadStations(categoryId);

        } else if (intent.hasExtra(REGION_EXTRA)) {
            regions = true;
            region = intent.getStringExtra(REGION_EXTRA);
            setStandartSelection();
            loadStations(region);

        } else {
            return;
        }


        getLoaderManager().restartLoader(Constants.SIMPLE_STATIONS_LOADER_ID, null, this);

        stationCursorAdapter = new StationCursorAdapter(this, R.layout.station_item);
        GridView gridView = (GridView) findViewById(R.id.gridView);
        gridView.setAdapter(stationCursorAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                SingleStationActivity.launch(StationsActivity.this, id);
            }
        });

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        sortOrderView = (TextView) findViewById(R.id.sortOrder);
        flagSortOrder = (ImageView) findViewById(R.id.flagSortOrder);
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        setStandartSelection();
        getLoaderManager().restartLoader(Constants.SIMPLE_STATIONS_LOADER_ID, null, this);
    }


    public void setStandartSelection() {
        if (regions) {
            selection = RadioStationContract.RadioStationEntry.COLUMN_REGION + " = ? ";
            selectionArgs = new String[]{region};
            sortOrder = RadioStationContract.RadioStationEntry.DEFAULT_SORT_ORDER;
        } else {
            selection = RadioStationContract.RadioStationEntry.COLUMN_CATEGORY_ID + " = ? ";
            selectionArgs = new String[]{categoryId + ""};
            sortOrder = RadioStationContract.RadioStationEntry.DEFAULT_SORT_ORDER;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (id > 0 && position > 0) {
            Category category = adapter.getItem(position);
            showSortView(category.getName());
        } else {
            removeSortOrder(null);
        }

        //haut nit hin weil ich bei "liststations for category" die "directories" nicht bekomme ...
        //laden und dazuspeichern
    }

    //passiert nur wenn kategorien angezeigt werden, no need to check if region display
    public void requeryChildCategories() {
        this.adapter.setList(CategoryHelper.listChildCategories(this, categoryId));
        this.adapter.notifyDataSetChanged();
        determineSpinnerVisibility();
    }

    public boolean determineSpinnerVisibility() {
        if (adapter == null | spinner == null) {
            return false;
        }
        if (adapter.getCount() <= 1) { //remove spinner if size is lte 1 bc 1 item is empty item...
            spinner.setVisibility(View.GONE);
            return false;
        } else {
            spinner.setVisibility(View.VISIBLE);
            return true;
        }
    }


    public void loadStations(String region) {
        Task task = new RadioStationTask();
        task.setTaskType(Task.TaskType.listContinentStations).setStringExtra(region);
        Intent intent = new Intent(this, NetworkService.class);
        intent.putExtra(NetworkService.SERVICE_ARG1, task);
        startService(intent);
    }

    public void loadStations(long categoryId) {
        Task task = new RadioStationTask();
        task.setTaskType(Task.TaskType.listCategoryStations).setLongExtra(categoryId);
        Intent intent = new Intent(this, NetworkService.class);
        intent.putExtra(NetworkService.SERVICE_ARG1, task);
        startService(intent);

        task = new CategoryNetworkTask();
        task.setTaskType(Task.TaskType.listChildCategories).setLongExtra(categoryId);
        intent = new Intent(this, NetworkService.class);
        intent.putExtra(NetworkService.SERVICE_ARG1, task);
        startService(intent);
    }

    public void showSortView(String string) {
        showSortView(string, -1);
    }

    public void showSortView(String string, int drawableId) {
        sortOrderView.setText("Filter By " + string);
        if (drawableId > 0) {
            try {
                flagSortOrder.setImageDrawable(getResources().getDrawable(drawableId));
            } catch (RuntimeException rte) {
                //TODO: FrameLayoutOverlay with TextView
            }
        }
        linearLayout.setVisibility(View.VISIBLE);
    }

    public void sortBy(String country, int drawableId) {
        showSortView(country, drawableId);
        if (regions) {
            selection = RadioStationContract.RadioStationEntry.COLUMN_REGION + " = ? AND " + RadioStationContract.RadioStationEntry.COLUMN_COUNTRY + " = ? ";
            selectionArgs = new String[]{region + "", country};
        } else {
            selection = RadioStationContract.RadioStationEntry.COLUMN_CATEGORY_ID + " = ? AND " + RadioStationContract.RadioStationEntry.COLUMN_COUNTRY + " = ? ";
            selectionArgs = new String[]{categoryId + "", country};
        }
        sortOrder = RadioStationContract.RadioStationEntry.DEFAULT_SORT_ORDER;
        getLoaderManager().restartLoader(Constants.SIMPLE_STATIONS_LOADER_ID, null, this);
    }

    public void removeSortOrder(View view) {
        selection = RadioStationContract.RadioStationEntry.COLUMN_CATEGORY_ID + " = ? ";
        selectionArgs = new String[]{categoryId + ""};
        sortOrder = RadioStationContract.RadioStationEntry.DEFAULT_SORT_ORDER;

        getLoaderManager().restartLoader(Constants.SIMPLE_STATIONS_LOADER_ID, null, this);
        linearLayout.setVisibility(View.GONE);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, RadioStationContract.RadioStationEntry.CONTENT_URI, null, selection, selectionArgs, sortOrder);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        stationCursorAdapter.swapCursor(data);
        stationCursorAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        stationCursorAdapter.swapCursor(null);
        stationCursorAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        try {
            getLoaderManager().destroyLoader(Constants.SIMPLE_STATIONS_LOADER_ID);
            getContentResolver().unregisterContentObserver(catObserver);
        } catch (NullPointerException npe) {
            //apparently this is a thing
        }
        super.onDestroy();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_stations;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public static void launch(Activity activity, long categoryId) {
        Intent intent = new Intent(activity, StationsActivity.class);
        intent.putExtra(CATEGORY_ID_EXTRA, categoryId);
        activity.startActivity(intent);
    }

    public static void launch(Activity activity, String country) {
        Intent intent = new Intent(activity, StationsActivity.class);
        intent.putExtra(REGION_EXTRA, country);
        activity.startActivity(intent);
    }

    public class CatContentObserver extends ContentObserver {

        public CatContentObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            requeryChildCategories();

        }
    }
}

package at.fibuszene.listendirble;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.audiofx.Visualizer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.MediaRouteActionProvider;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.RemoteMediaPlayer;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.io.IOException;

import api.StationsApi;
import fragment.StationDetailsFragment;
import fragment.StationFragment;
import model.RadioStation;
import model.SongHistoryItem;
import persistency.helper.RadioStationHelper;
import retrofit.RestAdapter;
import service.PlaybackService;
import utils.Constants;

/*
    Holy shit one bloated class, on the top of my refactoring list
 */
public class SingleStationActivity extends BaseActivity {
    public static final String STATION_ID = "at.fibuszene.listendirble.station_id";
    private RadioStation station;

    //cast
    private MediaRouter mMediaRouter;
    private MediaRouteSelector mMediaRouteSelector;
    private MediaRouter.Callback mMediaRouterCallback;
    private CastDevice mSelectedDevice;
    private GoogleApiClient mApiClient;
    private RemoteMediaPlayer mRemoteMediaPlayer;
    private Cast.Listener mCastClientListener;
    private boolean mWaitingForReconnect = false;
    private boolean mApplicationStarted = false;
    private boolean mVideoIsLoaded;
    private boolean mIsPlaying;
    private double volumen;
    //cast

    private AudioManager am;
    private AsyncTask currentTask;
    private CustomHandler handler;
    private StationsApi api;
    private boolean isRunning, details;
    private Messenger messenger;
    private ProgressBar bar;
    private StationFragment stationFragment;
    private StationDetailsFragment detailsFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (intent == null || !intent.hasExtra(STATION_ID)) {
            return;
        }
        long stationID = intent.getLongExtra(STATION_ID, 0);
        station = RadioStation.from(RadioStationHelper.get(this, stationID));
        am = (AudioManager) getSystemService(AUDIO_SERVICE);
        api = new RestAdapter.Builder().setEndpoint(Constants.ENDPOINT).build().create(StationsApi.class);
        handler = new CustomHandler();
        messenger = new Messenger(handler);
        bar = new ProgressBar(this);
        bar.setVisibility(View.GONE);
        toolbar.addView(bar);

        detailsFragment = StationDetailsFragment.getInstance();
        stationFragment = StationFragment.getInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.headerFrameLayout, stationFragment).commit();
        setFragmentStations();
        loadStation(stationID);
        setCurrentSong(stationFragment.getCurrentSong());

        initMediaRouter();
    }

    public RadioStation getStation() {
        return station;
    }

    public void setCurrentSong(SongHistoryItem songHistoryItem) {
        stationFragment.setCurrentSong(songHistoryItem);
    }

    public void loadStation(final long stationID) {

        currentTask = new AsyncTask<Void, Void, RadioStation>() {

            @Override
            protected RadioStation doInBackground(Void... params) {
                if (isCancelled()) {
                    Log.d("Something", "Still running");
                    return null;
                }
                RadioStation station = null;
                try {
                    station = api.getStation(Constants.API_KEY, stationID);
                    Log.d("SingleStationActivity", station + "");
                    Log.d("Something", "Still doing my thing doInBackground " + isCancelled());
                } catch (RuntimeException ste) {
                    ste.printStackTrace();
                    station = null;
                    //SocketTimeoutException
                }
                return station;
            }


            @Override
            protected void onPostExecute(RadioStation radioStation) {
                super.onPostExecute(radioStation);
                Log.d("Something", "Still doing my thing onPostExecute " + isCancelled());

                if (radioStation != null && !isCancelled()) {
                    station = radioStation;
//                    RadioStationHelper.saveOrUpdate(SingleStationActivity.this, station);
                    stationFragment.newSongHistory(radioStation.getSongHistory());
                    setFragmentStations();
                    //wuerd passen sobald lied fertig neu laden fuer neues lied
                    //fuehrt aber zu loop auch nachdem die activity weg ist.
//                    setCurrentSong(adapter.getItem(0));
                }
            }
        }.execute();
    }

    public void play(View view) {
        if (!mVideoIsLoaded) {
            startVideo();
        } else {
            controlVideo();
        }

        if (isRunning) {
            pause(view);
        } else {
            String oe3 = "http://ms01.oe3.fm/oe3metafiles/Player/Player.html";
            String url = "http://radio.orf.at/player/radioplayer.html?station=fm4";
            Intent intent = new Intent(this, PlaybackService.class);
            intent.setAction(PlaybackService.SERVICE_PLAY_ACTION);
            intent.putExtra(PlaybackService.SERVICE_ARG1_URL, station.getStreamurl());
            intent.putExtra(PlaybackService.SERVICE_ARG2_STATION_ID, station.getId());
            intent.putExtra(PlaybackService.SERVICE_ARG3_MESSENGER, messenger);
            startService(intent);
            displayLoadingAnimation(PlaybackService.MessageType.loading);
        }
        this.isRunning = !isRunning;
        view.setActivated(isRunning);
    }

    public boolean isPlaying() {
        return isRunning;
    }

    public void pause(View view) {
        Intent intent = new Intent(this, PlaybackService.class);
        intent.setAction(PlaybackService.SERVICE_PAUSE_ACTION);
        startService(intent);
    }

    public void stationInfo(View view) {
        if (!details) {
            getSupportFragmentManager().beginTransaction().replace(R.id.headerFrameLayout, detailsFragment).addToBackStack("details").commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.headerFrameLayout, stationFragment).addToBackStack("header").commit();
        }
        setFragmentStations();
        details = !details;
    }

    public void openInBrowser(View view) {

    }


    public void favorite(View view) {
        if (RadioStationHelper.updateFavorite(this, station.getId(), station.toggleFavorite())) {
            detailsFragment.toggleFavorite(station.isFavorite());
        }
    }

    public void cancelTask() {
        if (currentTask != null) {
            boolean canceld = currentTask.cancel(true);
            Log.d("Something", "Still running cancelTask" + canceld);
        }
        if (handler != null) {
            handler.removeCallbacks(null);
        }
    }


    public void setFragmentStations() {
        if (detailsFragment != null) {
            detailsFragment.stationLoaded(station);
        }
        if (stationFragment != null) {
            stationFragment.stationLoaded(station);
        }
    }

    @Override
    public void onBackPressed() {
        this.details = !this.details;
        setFragmentStations();
        super.onBackPressed();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_single_station;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            cancelTask();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_DOWN:
            case KeyEvent.KEYCODE_VOLUME_UP:
                volumen = am.getStreamVolume(AudioManager.STREAM_MUSIC);
                try {
                    mCastClientListener.onVolumeChanged();
                } catch (NullPointerException npe) {
                    //if no chromecast is connected
                }
                break;
        }

        return super.onKeyDown(keyCode, event);
    }

    public static void launch(Activity activity, long stationId) {
        Intent intent = new Intent(activity, SingleStationActivity.class);
        intent.putExtra(STATION_ID, stationId);
        activity.startActivity(intent);
    }

    private void initMediaRouter() {
        // Configure Cast device discovery
        mMediaRouter = MediaRouter.getInstance(getApplicationContext());
        mMediaRouteSelector = new MediaRouteSelector.Builder()
                .addControlCategory(
                        CastMediaControlIntent.categoryForCast(CastMediaControlIntent.DEFAULT_MEDIA_RECEIVER_APPLICATION_ID))
                .build();
        mMediaRouterCallback = new MediaRouterCallback();
    }


    private void initCastClientListener() {
        mCastClientListener = new Cast.Listener() {
            @Override
            public void onApplicationStatusChanged() {

            }

            @Override
            public void onVolumeChanged() {
                mRemoteMediaPlayer.setStreamVolume(mApiClient, volumen);
            }

            @Override
            public void onApplicationDisconnected(int statusCode) {
                teardown();
            }
        };
    }

    private void initRemoteMediaPlayer() {
        mRemoteMediaPlayer = new RemoteMediaPlayer();
        mRemoteMediaPlayer.setOnStatusUpdatedListener(new RemoteMediaPlayer.OnStatusUpdatedListener() {
            @Override
            public void onStatusUpdated() {
                MediaStatus mediaStatus = mRemoteMediaPlayer.getMediaStatus();
                mIsPlaying = mediaStatus.getPlayerState() == MediaStatus.PLAYER_STATE_PLAYING;
            }
        });

        mRemoteMediaPlayer.setOnMetadataUpdatedListener(new RemoteMediaPlayer.OnMetadataUpdatedListener() {
            @Override
            public void onMetadataUpdated() {
            }
        });
    }

    private void controlVideo() {
        if (mRemoteMediaPlayer == null || !mVideoIsLoaded)
            return;

        if (mIsPlaying) {
            mRemoteMediaPlayer.pause(mApiClient);
        } else {
            mRemoteMediaPlayer.play(mApiClient);
        }
    }

    private void startVideo() {
        MediaMetadata mediaMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MUSIC_TRACK);
        mediaMetadata.putString(MediaMetadata.KEY_TITLE, station.getName());
        mediaMetadata.putString(MediaMetadata.KEY_ALBUM_TITLE, station.getDescription());

        String type = "audio/mp3";
        try {
            type = station.getStreams().get(0).getType();
        } catch (IndexOutOfBoundsException | NullPointerException npe) {
            Log.d("StreamTypes SingleStation", station.getStreams() + "");
        }
        Log.d("StreamTypes SingleStation", type);

        MediaInfo mediaInfo = new MediaInfo.Builder(station.getStreamurl())
                .setContentType(type)
                .setStreamType(MediaInfo.STREAM_TYPE_NONE)
                .setMetadata(mediaMetadata)
                .build();
        try {
            mRemoteMediaPlayer.load(mApiClient, mediaInfo, true)
                    .setResultCallback(new ResultCallback<RemoteMediaPlayer.MediaChannelResult>() {
                        @Override
                        public void onResult(RemoteMediaPlayer.MediaChannelResult mediaChannelResult) {
                            if (mediaChannelResult.getStatus().isSuccess()) {
                                mVideoIsLoaded = true;
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Start media router discovery
        mMediaRouter.addCallback(mMediaRouteSelector, mMediaRouterCallback, MediaRouter.CALLBACK_FLAG_PERFORM_ACTIVE_SCAN);
    }

    @Override
    protected void onPause() {
        if (isFinishing()) {
            // End media router discovery
            mMediaRouter.removeCallback(mMediaRouterCallback);
        }
        cancelTask();

        super.onPause();
    }

    private class MediaRouterCallback extends MediaRouter.Callback {

        @Override
        public void onRouteSelected(MediaRouter router, MediaRouter.RouteInfo info) {
            initCastClientListener();
            initRemoteMediaPlayer();
            mSelectedDevice = CastDevice.getFromBundle(info.getExtras());
            launchReceiver();
        }

        @Override
        public void onRouteUnselected(MediaRouter router, MediaRouter.RouteInfo info) {
            teardown();
            mSelectedDevice = null;
            mVideoIsLoaded = false;
        }
    }

    private void launchReceiver() {
        Cast.CastOptions.Builder apiOptionsBuilder = Cast.CastOptions
                .builder(mSelectedDevice, mCastClientListener);

        ConnectionCallbacks mConnectionCallbacks = new ConnectionCallbacks();
        ConnectionFailedListener mConnectionFailedListener = new ConnectionFailedListener();
        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(Cast.API, apiOptionsBuilder.build())
                .addConnectionCallbacks(mConnectionCallbacks)
                .addOnConnectionFailedListener(mConnectionFailedListener)
                .build();

        mApiClient.connect();
    }

    private class ConnectionCallbacks implements GoogleApiClient.ConnectionCallbacks {

        @Override
        public void onConnected(Bundle hint) {
            if (mWaitingForReconnect) {
                mWaitingForReconnect = false;
                reconnectChannels(hint);
            } else {
                try {
                    Cast.CastApi.launchApplication(mApiClient, CastMediaControlIntent.DEFAULT_MEDIA_RECEIVER_APPLICATION_ID, false)
                            .setResultCallback(new ResultCallback<Cast.ApplicationConnectionResult>() {
                                @Override
                                public void onResult(Cast.ApplicationConnectionResult applicationConnectionResult) {
                                    Status status = applicationConnectionResult.getStatus();
                                    if (status.isSuccess()) {
                                        //Values that can be useful for storing/logic
                                        ApplicationMetadata applicationMetadata = applicationConnectionResult.getApplicationMetadata();
                                        String sessionId = applicationConnectionResult.getSessionId();
                                        String applicationStatus = applicationConnectionResult.getApplicationStatus();
                                        boolean wasLaunched = applicationConnectionResult.getWasLaunched();

                                        mApplicationStarted = true;
                                        reconnectChannels(null);
                                    }
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onConnectionSuspended(int i) {
            mWaitingForReconnect = true;
        }
    }

    private void reconnectChannels(Bundle hint) {
        if ((hint != null) && hint.getBoolean(Cast.EXTRA_APP_NO_LONGER_RUNNING)) {
            Log.e("Chromecast", "App is no longer running");
            teardown();
        } else {
            try {
                Cast.CastApi.setMessageReceivedCallbacks(mApiClient, mRemoteMediaPlayer.getNamespace(), mRemoteMediaPlayer);
            } catch (IOException e) {
                e.printStackTrace();

                Log.e("Chromecast", "Exception while creating media channel ", e);
            } catch (NullPointerException e) {
                e.printStackTrace();

                Log.e("Chromecast", "Something wasn't reinitialized for reconnectChannels");
            }
        }
    }

    private class ConnectionFailedListener implements GoogleApiClient.OnConnectionFailedListener {
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            teardown();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem mediaRouteMenuItem = menu.findItem(R.id.media_route_menu_item);
        MediaRouteActionProvider mediaRouteActionProvider = (MediaRouteActionProvider) MenuItemCompat.getActionProvider(mediaRouteMenuItem);
        mediaRouteActionProvider.setRouteSelector(mMediaRouteSelector);
        return true;
    }


    private void teardown() {
        if (mApiClient != null) {
            if (mApplicationStarted) {
                try {
                    Cast.CastApi.stopApplication(mApiClient);
                    if (mRemoteMediaPlayer != null) {
                        Cast.CastApi.removeMessageReceivedCallbacks(mApiClient, mRemoteMediaPlayer.getNamespace());
                        mRemoteMediaPlayer = null;
                    }
                } catch (IOException e) {
                    Log.e("Chromecast", "Exception while removing application " + e);
                }
                mApplicationStarted = false;
            }
            if (mApiClient.isConnected())
                mApiClient.disconnect();
            mApiClient = null;
        }
        mSelectedDevice = null;
        mVideoIsLoaded = false;
    }


    @Override
    public void onDestroy() {
        teardown();
        cancelTask();
        super.onDestroy();
    }

    public void displayLoadingAnimation(PlaybackService.MessageType type) {
        if (bar == null) {
            return;
        }
        switch (type) {
            case loading:
                bar.setVisibility(View.VISIBLE);
                break;
            case started:
                bar.setVisibility(View.GONE);

                break;
            case error:
                Toast.makeText(this, "error loading station", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public class CustomHandler extends Handler {
        @Override
        public void handleMessage(final Message msg) {
            super.handleMessage(msg);
            PlaybackService.MessageType type = PlaybackService.MessageType.valueOf(msg.arg1);
            displayLoadingAnimation(type);
            if (type == PlaybackService.MessageType.started) {

                Visualizer visualizer = new Visualizer(msg.arg2);
                visualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);

                visualizer.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
                    public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                        stationFragment.update(bytes);
                    }

                    public void onFftDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
                    }
                }, Visualizer.getMaxCaptureRate() / 2, true, false);
                visualizer.setEnabled(true);

            }
        }
    }

}

package fragment;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.wefika.flowlayout.FlowLayout;

import at.fibuszene.listendirble.R;
import at.fibuszene.listendirble.SingleStationActivity;
import callback.StationCallback;
import model.Directory;
import model.Image;
import model.RadioStation;

/**
 * Created by benedikt.
 */
public class StationDetailsFragment extends Fragment implements StationCallback {
    private TextView stationName, stationDescription;
    private ImageView stationImage, countryPic;
    private FlowLayout categoryFlowLayout;
    private ImageButton favoriteButton;

    public static StationDetailsFragment getInstance() {
        StationDetailsFragment fragment = new StationDetailsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_station_details, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        stationName = (TextView) view.findViewById(R.id.stationName);
        stationDescription = (TextView) view.findViewById(R.id.stationDescription);
        stationImage = (ImageView) view.findViewById(R.id.stationImage);
        countryPic = (ImageView) view.findViewById(R.id.countryPic);
        categoryFlowLayout = (FlowLayout) view.findViewById(R.id.categoryFlowLayout);
        favoriteButton = (ImageButton) view.findViewById(R.id.favoriteButton);
        try {
            stationLoaded(((SingleStationActivity) getActivity()).getStation());
        } catch (ClassCastException cce) {
            //nothing much to do
        }
    }

    @Override
    public void stationLoaded(RadioStation station) {
        if (isAdded() && station != null) {
            stationName.setText(station.getName());
            stationDescription.setText(station.getDescription());
            String pic = station.getImage();

            if (pic == null || TextUtils.isEmpty(pic)) {
                stationImage.setVisibility(View.GONE);
            } else {
                stationImage.setImageBitmap(BitmapFactory.decodeFile(station.getImage()));
            }
            try {
                final int id = getActivity().getResources().getIdentifier(station.getCountry().toLowerCase(), "drawable", getActivity().getPackageName());
                countryPic.setImageDrawable(getActivity().getResources().getDrawable(id));
            } catch (RuntimeException rte) {
                //TODO: FrameLayoutOverlay with TextView
            }
            TextView txtView = null;
            for (Directory tmp : station.getDirectories()) {
                txtView = new TextView(getActivity());
                txtView.setText(tmp.getName());
                categoryFlowLayout.addView(txtView);
            }
            favoriteButton.setActivated(station.isFavorite());
        }
    }

    public void toggleFavorite(boolean favorite) {
        favoriteButton.setActivated(favorite);
    }

}

package fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import adapter.SongHistoryAdapter;
import at.fibuszene.listendirble.MainActivity;
import at.fibuszene.listendirble.R;
import at.fibuszene.listendirble.SingleStationActivity;
import callback.StationCallback;
import model.RadioStation;
import model.SongHistoryItem;
import utils.VisualizerView;

/**
 * Created by benedikt.
 */
public class StationFragment extends Fragment implements StationCallback {
    private SongHistoryAdapter adapter;
    private RadioStation station;
    private ImageView banner;
    private VisualizerView visualizerView;
    private TextView stationName;
    private ImageButton playButton;
    private boolean bannerIsStationImage;


    public static StationFragment getInstance() {
        StationFragment fragment = new StationFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_station_header, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        banner = (ImageView) view.findViewById(R.id.bannerImageView);
        stationName = (TextView) view.findViewById(R.id.stationName);
        visualizerView = (VisualizerView) view.findViewById(R.id.bannerVisualizerView);
        playButton = (ImageButton) view.findViewById(R.id.playButton);

        ListView listView = (ListView) view.findViewById(R.id.songhistoryList);
        listView.setEmptyView(view.findViewById(R.id.empty_list_view));
        adapter = new SongHistoryAdapter(getActivity());
        listView.setAdapter(adapter);
        initView();
    }

    public SongHistoryItem getCurrentSong() {
        try {
            return adapter.getItem(0);
        } catch (IndexOutOfBoundsException | NullPointerException npe) {
            return null;
        }
    }

    @Override
    public void stationLoaded(RadioStation station) {
        this.station = station;
        initView();
    }

    public void initView() {
        if (isAdded() && station != null) {
            stationName.setTypeface(MainActivity.HEADING_FONT);
            stationName.setText(station.getName());
            playButton.setActivated(((SingleStationActivity) getActivity()).isPlaying());
            if (station.getSongHistory() != null) {
                newSongHistory(station.getSongHistory());
            }
            this.adapter.notifyDataSetChanged();
        }
    }

    public void bannerIsStationImage() {
        if (station != null) {
            String stationImage = station.getImage();
            if (stationImage != null && !TextUtils.isEmpty(stationImage)) {
                Picasso.with(getActivity()).load(stationImage).into(banner);
                bannerIsStationImage = true;
            }
        }
    }

    public void setCurrentSong(SongHistoryItem songHistoryItem) {
        if (songHistoryItem == null) {
            return;
        }
        if (!bannerIsStationImage) {
            Picasso.with(getActivity()).load(songHistoryItem.getImage()).into(banner);
        }

//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                loadStation(stationID);
//            }
//        }, songHistoryItem.getDate().getUsec());
        //TODO: sync not time from load but time from load- time elapsed
    }

    public void update(byte[] bytes) {
        this.visualizerView.updateVisualizer(bytes);
    }

    public void newSongHistory(List<SongHistoryItem> songHistory) {
        this.adapter.addAll(songHistory);
    }
}

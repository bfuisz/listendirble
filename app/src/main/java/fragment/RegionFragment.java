package fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import adapter.RegionAdapter;
import at.fibuszene.listendirble.R;
import at.fibuszene.listendirble.StationsActivity;

/**
 * Created by benedikt.
 */
public class RegionFragment extends Fragment implements AdapterView.OnItemClickListener {
    private RegionAdapter adapter;

    public static Fragment getInstance() {
        RegionFragment fragment = new RegionFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return View.inflate(getActivity(), R.layout.fragment_country, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GridView layout = (GridView) view.findViewById(R.id.gridView);
        adapter = new RegionAdapter(getActivity());
        layout.setAdapter(adapter);
        layout.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        StationsActivity.launch(getActivity(), adapter.getItem(position));
    }
}

package fragment;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import adapter.GridViewAdapter;
import at.fibuszene.listendirble.R;
import at.fibuszene.listendirble.StationsActivity;
import persistency.contracts.CategoryContract;
import persistency.contracts.RadioStationContract;
import service.NetworkService;
import service.tasks.CategoryNetworkTask;
import service.tasks.Task;
import utils.Constants;

/**
 * Created by benedikt.
 */
public class CategoryFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    public static final String RELOAD_CATEGORIES = "at.fibuszene.listendirble.reload_categories";
    private GridViewAdapter gridViewAdapter;


    public static Fragment getInstance() {
        CategoryFragment fragment = new CategoryFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return View.inflate(getActivity(), R.layout.fragment_category, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        gridViewAdapter = new GridViewAdapter(getActivity(), R.layout.category_item);
        GridView gridView = (GridView) view.findViewById(R.id.gridView);
        gridView.setAdapter(gridViewAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                StationsActivity.launch(getActivity(), id);
            }
        });
        getActivity().getLoaderManager().restartLoader(Constants.CATEGORY_LOADER, null, this);
        boolean reload = true;
        if (savedInstanceState != null && savedInstanceState.containsKey(RELOAD_CATEGORIES)) {
            reload = savedInstanceState.getBoolean(RELOAD_CATEGORIES);
        }
        if (reload) {
            loadCatgegories();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), CategoryContract.CategoryEntry.CONTENT_URI, null, CategoryContract.CategoryEntry.COLUMN_PARENT + " = 0", null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        gridViewAdapter.swapCursor(data);
        gridViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        gridViewAdapter.swapCursor(null);
        gridViewAdapter.notifyDataSetChanged();
    }


    public void loadCatgegories() {
        Task task = new CategoryNetworkTask();
        task.setTaskType(Task.TaskType.listPrimaryCategories);
        Intent intent = new Intent(getActivity(), NetworkService.class);
        intent.putExtra(NetworkService.SERVICE_ARG1, task);
        getActivity().startService(intent);
    }


}

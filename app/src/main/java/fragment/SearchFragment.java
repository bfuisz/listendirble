package fragment;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;


import java.util.List;

import adapter.CustomSpinnerAdapter;
import adapter.StationsArrayAdapter;
import api.StationsApi;
import at.fibuszene.listendirble.R;
import at.fibuszene.listendirble.SingleStationActivity;
import model.Category;
import model.RadioStation;
import persistency.helper.CategoryHelper;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import utils.Constants;

/**
 * Created by benedikt.
 */
public class SearchFragment extends Fragment implements View.OnClickListener, Callback<List<RadioStation>>, AdapterView.OnItemClickListener {
    private StationsApi api;
    private AutoCompleteTextView searchTextView;
    private Spinner categorySpinner;
    private ImageButton searchButton, expandSearch;
    private StationsArrayAdapter stationsArrayAdapter;
    private CustomSpinnerAdapter spinnerAdapter;
    private ProgressBar progressSpinner;
    private boolean expand;


    public static Fragment getInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.api = new RestAdapter.Builder().setEndpoint(Constants.ENDPOINT).setLogLevel(RestAdapter.LogLevel.BASIC).build().create(StationsApi.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return View.inflate(getActivity(), R.layout.fragment_search, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressSpinner = (ProgressBar) view.findViewById(R.id.progressSpinner);
        searchTextView = (AutoCompleteTextView) view.findViewById(R.id.searchField);

        searchButton = (ImageButton) view.findViewById(R.id.searchButton);
        expandSearch = (ImageButton) view.findViewById(R.id.expandSearch);

        categorySpinner = (Spinner) view.findViewById(R.id.categorySpinner);
        spinnerAdapter = new CustomSpinnerAdapter(getActivity(), CategoryHelper.listChildCategories(getActivity(), 0));//where parentId = 0, if(parentid == 0) it is a which is what i want primaryCategory
        categorySpinner.setAdapter(spinnerAdapter);

        GridView gridView = (GridView) view.findViewById(R.id.gridView);
        stationsArrayAdapter = new StationsArrayAdapter(getActivity());
        gridView.setAdapter(stationsArrayAdapter);
        gridView.setOnItemClickListener(this);

        searchButton.setOnClickListener(this);
        expandSearch.setOnClickListener(this);
    }


    public void expand() {
        categorySpinner.setVisibility(View.VISIBLE);
        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        categorySpinner.measure(widthSpec, heightSpec);
        ValueAnimator mAnimator = slideAnimator(0, categorySpinner.getMeasuredHeight());
        mAnimator.start();
    }

    public void collapse() {
        int finalHeight = categorySpinner.getHeight();
        ValueAnimator mAnimator = slideAnimator(finalHeight, 0);
        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                categorySpinner.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }

        });
        mAnimator.start();
    }

    private ValueAnimator slideAnimator(int start, int end) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = categorySpinner.getLayoutParams();
                layoutParams.height = value;
                categorySpinner.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SingleStationActivity.launch(getActivity(), id);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.expandSearch) {
            if (expand) {
                collapse();
            } else {
                expand();
            }
            this.expand = !expand;

        } else if (v.getId() == R.id.searchButton) {
            String spinnerSelection = null;
            try {
                spinnerSelection = ((Category) categorySpinner.getSelectedItem()).getName();//vermutlich eine Categorie yep called it...
            } catch (NullPointerException npe) {
                //Nullcheck is a couple of lines down
            }
            String searchText = searchTextView.getText() + "";
            if (!TextUtils.isEmpty(searchText)) {
                progressSpinner.setVisibility(View.VISIBLE);
                if (spinnerSelection != null && !TextUtils.isEmpty(spinnerSelection)) {
                    //apparently case sensitive
                    api.search(Constants.API_KEY, searchText, spinnerSelection.toLowerCase(), this);
                } else {
                    api.search(Constants.API_KEY, searchText, this);
                }
            }
        }
    }

    @Override
    public void success(List<RadioStation> radioStations, Response response) {
        this.stationsArrayAdapter.setList(radioStations);
        this.stationsArrayAdapter.notifyDataSetChanged();
        progressSpinner.setVisibility(View.GONE);
    }

    @Override
    public void failure(RetrofitError error) {
        progressSpinner.setVisibility(View.GONE);
    }
}

package persistency.contracts;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by benedikt.
 */
public class RadioStationContract {

    public static final String CONTENT_AUTHORITY = "at.fibuszene.listendirbel";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_RADIO_STATION = "radio_station";

    public static final class RadioStationEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_RADIO_STATION).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + "/" + PATH_RADIO_STATION;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_AUTHORITY + "/" + PATH_RADIO_STATION;

        public static final String TABLE_NAME = "radio_station";

        public static final String COLUMN_CATEGORY_ID = "categoryId";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_STREAM_URL = "streamurl";
        public static final String COLUMN_BIT_RATE = "bitrate";
        public static final String COLUMN_STATUS = "status";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_COUNTRY = "country";
        public static final String COLUMN_ADDED = "added";
        public static final String COLUMN_URL_ID = "urlid";
        public static final String COLUMN_WEBSITE = "website";
        public static final String COLUMN_IMAGE = "image";
        public static final String COLUMN_FAVORITE = "favorite";
        public static final String COLUMN_REGION = "region";

        public static final int COLUMN_ID_INDEX = 0;
        public static final int COLUMN_CATGEGORY_ID_INDEX = 1;
        public static final int COLUMN_NAME_INDEX = 2;
        public static final int COLUMN_STREAM_URL_INDEX = 3;
        public static final int COLUMN_BIT_RATE_INDEX = 4;
        public static final int COLUMN_STATUS_INDEX = 5;
        public static final int COLUMN_DESCRIPTION_INDEX = 6;
        public static final int COLUMN_COUNTRY_INDEX = 7;
        public static final int COLUMN_ADDED_INDEX = 8;
        public static final int COLUMN_URL_ID_INDEX = 9;
        public static final int COLUMN_WEBSITE_INDEX = 10;
        public static final int COLUMN_IMAGE_INDEX = 11;
        public static final int COLUMN_FAVORITE_INDEX = 12;
        public static final int COLUMN_REGION_INDEX = 13;

        public static final String DEFAULT_SORT_ORDER = COLUMN_NAME;

        public static Uri buildIdURI(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildDateURI(String date) {
            return CONTENT_URI.buildUpon().appendPath(date).build();
        }

    }
}

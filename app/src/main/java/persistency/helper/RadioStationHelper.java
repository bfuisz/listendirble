package persistency.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import at.fibuszene.listendirble.SingleStationActivity;
import model.RadioStation;
import persistency.contracts.CategoryContract;
import persistency.contracts.RadioStationContract;


/**
 * Created by benedikt.
 */
public class RadioStationHelper {

    public int stationAmount(Context context) {
        Cursor cursor = context.getContentResolver().query(RadioStationContract.RadioStationEntry.CONTENT_URI, null, null, null, null);
        if (cursor.moveToFirst()) {
            return cursor.getCount();
        }
        return -1;
    }

    public static void saveOrUpdate(Context context, RadioStation radioStation) {
        if (radioStation != null) {

            if (radioStation.getId() > 0) {
                update(context, radioStation);
            } else {
                save(context, radioStation);
            }
        }
    }

    private static Uri save(Context context, RadioStation radioStation) {
        return context.getContentResolver().insert(RadioStationContract.RadioStationEntry.CONTENT_URI, radioStation.values());
    }

    private static int update(Context context, RadioStation radioStation) {
        return context.getContentResolver().update(RadioStationContract.RadioStationEntry.CONTENT_URI, radioStation.values(), RadioStationContract.RadioStationEntry._ID + " = ? ", new String[]{radioStation.getId() + ""});
    }

    public static Cursor get(Context context, long radioStationId) {
        Cursor cursor = context.getContentResolver().query(RadioStationContract.RadioStationEntry.CONTENT_URI, null, RadioStationContract.RadioStationEntry._ID + " = ?", new String[]{radioStationId + ""}, null);
        cursor.moveToFirst();
        return cursor;
    }

    public static Cursor get(Context context, RadioStation radioStation) {
        if (radioStation != null) {
            return get(context, radioStation.getId());
        }
        return null;
    }

    public static Cursor list(Context context) {
        try {
            Cursor cursor = context.getContentResolver().query(RadioStationContract.RadioStationEntry.CONTENT_URI, null, null, null, null);
            cursor.moveToFirst();
            return cursor;
        } catch (NullPointerException npe) {
            return null;
        }
    }

    public static List<RadioStation> list(Context context, String selection, String[] selectionArgs) {
        Cursor cursor = RadioStationHelper.list(context, RadioStationContract.RadioStationEntry.COLUMN_FAVORITE + " = 1", null, RadioStationContract.RadioStationEntry.DEFAULT_SORT_ORDER);
        List<RadioStation> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                list.add(RadioStation.from(cursor));
                cursor.moveToNext();
            }
        }
        return list;
    }

    public static Cursor list(Context context, String selection, String[] selectionArgs, String sortOrder) {
        try {
            Cursor cursor = context.getContentResolver().query(RadioStationContract.RadioStationEntry.CONTENT_URI, null, selection, selectionArgs, sortOrder);
            cursor.moveToFirst();
            return cursor;
        } catch (NullPointerException npe) {
            return null;
        }
    }

    public static int delete(Context context, RadioStation radioStation) {
        if (radioStation != null) {
            return delete(context, radioStation.getId());
        }
        return -1;
    }

    public static int delete(Context context, long radioStationId) {
        if (radioStationId > 0) {
            return context.getContentResolver().delete(RadioStationContract.RadioStationEntry.CONTENT_URI, RadioStationContract.RadioStationEntry._ID + " = ? ", new String[]{radioStationId + ""});
        }
        return -1;
    }

    public static boolean updateFavorite(Context context, long id, boolean favorite) {
        ContentValues values = new ContentValues();
        values.put(RadioStationContract.RadioStationEntry.COLUMN_FAVORITE, favorite);
        return context.getContentResolver().update(RadioStationContract.RadioStationEntry.CONTENT_URI, values, RadioStationContract.RadioStationEntry._ID + " = ? ", new String[]{id + ""}) > 0;
    }
}

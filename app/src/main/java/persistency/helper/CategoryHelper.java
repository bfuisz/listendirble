package persistency.helper;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

import at.fibuszene.listendirble.StationsActivity;
import model.Category;
import persistency.contracts.CategoryContract;


/**
 * Created by benedikt.
 */
public class CategoryHelper {

    public static void saveOrUpdate(Context context, Category category) {
        if (category != null) {
            if (category.getId() > 0) {
                update(context, category);
            } else {
                save(context, category);
            }
        }
    }

    private static Uri save(Context context, Category category) {
        return context.getContentResolver().insert(CategoryContract.CategoryEntry.CONTENT_URI, category.values());
    }

    private static int update(Context context, Category category) {
        return context.getContentResolver().update(CategoryContract.CategoryEntry.CONTENT_URI, category.values(), CategoryContract.CategoryEntry._ID + " = ? ", new String[]{category.getId() + ""});
    }

    public static Cursor get(Context context, long categoryId) {
        return context.getContentResolver().query(CategoryContract.CategoryEntry.CONTENT_URI, null, CategoryContract.CategoryEntry._ID, new String[]{categoryId + ""}, null);
    }

    public static Cursor get(Context context, Category category) {
        if (category != null) {
            return get(context, category.getId());
        }
        return null;
    }

    public static Cursor list(Context context) {
        try {
            return context.getContentResolver().query(CategoryContract.CategoryEntry.CONTENT_URI, null, null, null, null);
        } catch (NullPointerException npe) {
            return null;
        }
    }

    public static Cursor list(Context context, String selection, String[] selectionArgs, String sortOrder) {
        try {
            return context.getContentResolver().query(CategoryContract.CategoryEntry.CONTENT_URI, null, selection, selectionArgs, sortOrder);
        } catch (NullPointerException npe) {
            return null;
        }
    }

    public static int delete(Context context, Category category) {
        if (category != null) {
            return delete(context, category.getId());
        }
        return -1;
    }

    public static int delete(Context context, long category) {
        if (category > 0) {
            return context.getContentResolver().delete(CategoryContract.CategoryEntry.CONTENT_URI, CategoryContract.CategoryEntry._ID + " = ? ", new String[]{category + ""});
        }
        return -1;
    }


    public static List<Category> listChildCategories(Context context, long categoryId) {
        Cursor cursor = context.getContentResolver().query(CategoryContract.CategoryEntry.CONTENT_URI, null, CategoryContract.CategoryEntry.COLUMN_PARENT + " = ? ", new String[]{categoryId + ""}, null);
        List<Category> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                list.add(Category.from(cursor));
                cursor.moveToNext();
            }
        }
        return list;
    }

}

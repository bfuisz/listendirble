package persistency;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import persistency.contracts.CategoryContract;
import persistency.contracts.RadioStationContract;


/**
 * Created by benedikt.
 */
public class DataContentProvider extends ContentProvider {
    private DatabaseHelper helper;
    private static final UriMatcher uriMatcher = buildUriMatcher();

    private static final int CATEGORY = 100;
    private static final int CATEGORY_ID = 101;

    private static final int RADIOSTATION = 200;
    private static final int RADIOSTATION_ID = 201;


    public static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        final String categoryAuthority = CategoryContract.CONTENT_AUTHORITY;
        matcher.addURI(categoryAuthority, CategoryContract.PATH_CATEGORY, CATEGORY);
        matcher.addURI(categoryAuthority, CategoryContract.PATH_CATEGORY + "/#", CATEGORY_ID);
        final String radioStationAuthority = RadioStationContract.CONTENT_AUTHORITY;
        matcher.addURI(radioStationAuthority, RadioStationContract.PATH_RADIO_STATION, RADIOSTATION);
        matcher.addURI(radioStationAuthority, RadioStationContract.PATH_RADIO_STATION + "/#", RADIOSTATION_ID);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        this.helper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor = null;
        String extra;
        switch (uriMatcher.match(uri)) {

            case CATEGORY:
                retCursor = helper.getReadableDatabase().query(
                        CategoryContract.CategoryEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;
            case CATEGORY_ID:
                extra = uri.getLastPathSegment();

                retCursor = helper.getReadableDatabase().query(
                        CategoryContract.CategoryEntry.TABLE_NAME,
                        projection,
                        CategoryContract.CategoryEntry._ID + " = ?",
                        new String[]{extra},
                        null,
                        null,
                        sortOrder);
                break;

            case RADIOSTATION:
                retCursor = helper.getReadableDatabase().query(
                        RadioStationContract.RadioStationEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;

            case RADIOSTATION_ID:
                extra = uri.getLastPathSegment();
                retCursor = helper.getReadableDatabase().query(
                        RadioStationContract.RadioStationEntry.TABLE_NAME,
                        projection,
                        RadioStationContract.RadioStationEntry._ID + " = ?",
                        new String[]{extra},
                        null,
                        null,
                        sortOrder);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        try {
            retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        } catch (NullPointerException npe) {
            //pretty much impossible
        }
        return retCursor;
    }


    @Override
    public String getType(Uri uri) {
        final int match = uriMatcher.match(uri);
        String type = null;
        switch (match) {
            case RADIOSTATION:
                type = RadioStationContract.RadioStationEntry.CONTENT_TYPE;
                break;
            case RADIOSTATION_ID:
                type = RadioStationContract.RadioStationEntry.CONTENT_ITEM_TYPE;
                break;
            case CATEGORY:
                type = CategoryContract.CategoryEntry.CONTENT_TYPE;
                break;
            case CATEGORY_ID:
                type = CategoryContract.CategoryEntry.CONTENT_ITEM_TYPE;
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return type;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = helper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        Uri returnUri = null;
        long _id = -1;

        switch (match) {
            case RADIOSTATION:
                _id = db.insert(RadioStationContract.RadioStationEntry.TABLE_NAME, null, values);
                if (_id > 0)
                    returnUri = RadioStationContract.RadioStationEntry.buildIdURI(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            case CATEGORY:
                _id = db.insert(CategoryContract.CategoryEntry.TABLE_NAME, null, values);
                if (_id > 0) {
                    returnUri = CategoryContract.CategoryEntry.buildIdURI(_id);
                }
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        final SQLiteDatabase db = helper.getWritableDatabase();
        final int match = uriMatcher.match(uri);

        int inserted = 0;
        long _id = -1;

        switch (match) {
            case RADIOSTATION:
                db.beginTransaction();
                for (ContentValues value : values) {
                    try {
                        _id = db.insertOrThrow(RadioStationContract.RadioStationEntry.TABLE_NAME, null, value);
                    } catch (SQLiteConstraintException sce) {
                        _id = -1;
                    }
                    if (_id > 0) {
                        inserted++;
                    }
                }
                db.setTransactionSuccessful();
                db.endTransaction();
                break;
            case CATEGORY:
                db.beginTransaction();
                for (ContentValues value : values) {
                    try {
                        _id = db.insertOrThrow(CategoryContract.CategoryEntry.TABLE_NAME, null, value);
                    } catch (SQLiteConstraintException sqLiteConstraintException) {
                        _id = -1;//already in db could be updated
                    }
                    if (_id > 0) {
                        inserted++;
                    }
                }
                db.setTransactionSuccessful();
                db.endTransaction();
                break;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return inserted;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = helper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        int rowsDeleted;
        switch (match) {
            case RADIOSTATION:
                rowsDeleted = db.delete(RadioStationContract.RadioStationEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case CATEGORY:
                rowsDeleted = db.delete(CategoryContract.CategoryEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        if (selection == null || rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = helper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        int rowsUpdated;

        switch (match) {
            case RADIOSTATION:
                rowsUpdated = db.update(RadioStationContract.RadioStationEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            case CATEGORY:
                rowsUpdated = db.update(CategoryContract.CategoryEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rowsUpdated;
    }

}

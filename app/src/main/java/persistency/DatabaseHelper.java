package persistency;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import persistency.contracts.CategoryContract;
import persistency.contracts.RadioStationContract;


/**
 * Created by benedikt.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "listen_dirble.db";
    private static final int DB_VERSION = 1;

    public static final String CREATE_CATEGORY_TABLE = "CREATE TABLE " + CategoryContract.CategoryEntry.TABLE_NAME + " (" +
            CategoryContract.CategoryEntry._ID + " INTEGER PRIMARY KEY, " +
            CategoryContract.CategoryEntry.COLUMN_NAME + " TEXT, " +
            CategoryContract.CategoryEntry.COLUMN_DESCRIPTION + " TEXT, " +
            CategoryContract.CategoryEntry.COLUMN_AMOUNT + " INTEGER, " +
            CategoryContract.CategoryEntry.COLUMN_PARENT + " INTEGER )";//eigene Tabelle mit foreign key fuer childcategories ? aber einziger unterschied ist amount...

    public static final String CREATE_RADIO_STATION_TABLE = "CREATE TABLE " + RadioStationContract.RadioStationEntry.TABLE_NAME + " (" +
            RadioStationContract.RadioStationEntry._ID + " INTEGER PRIMARY KEY, " +
            RadioStationContract.RadioStationEntry.COLUMN_CATEGORY_ID + " INTEGER , " +
            RadioStationContract.RadioStationEntry.COLUMN_NAME + " TEXT , " +
            RadioStationContract.RadioStationEntry.COLUMN_STREAM_URL + " TEXT , " +
            RadioStationContract.RadioStationEntry.COLUMN_BIT_RATE + " varchar(50) , " +
            RadioStationContract.RadioStationEntry.COLUMN_STATUS + " INTEGER , " +
            RadioStationContract.RadioStationEntry.COLUMN_DESCRIPTION + " TEXT , " +
            RadioStationContract.RadioStationEntry.COLUMN_COUNTRY + " TEXT , " +
            RadioStationContract.RadioStationEntry.COLUMN_ADDED + " TEXT , " +
            RadioStationContract.RadioStationEntry.COLUMN_URL_ID + " TEXT , " +
            RadioStationContract.RadioStationEntry.COLUMN_WEBSITE + " TEXT, " +
            RadioStationContract.RadioStationEntry.COLUMN_IMAGE + " TEXT, " +
            RadioStationContract.RadioStationEntry.COLUMN_FAVORITE + " INTEGER, " +
            RadioStationContract.RadioStationEntry.COLUMN_REGION + " varchar(50) )";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_CATEGORY_TABLE);
        db.execSQL(CREATE_RADIO_STATION_TABLE);
        db.execSQL("PRAGMA foreign_keys=ON;");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            db.execSQL("DROP IF EXISTS TABLE " + CategoryContract.CategoryEntry.TABLE_NAME);
            db.execSQL("DROP IF EXISTS TABLE " + RadioStationContract.RadioStationEntry.TABLE_NAME);
            onCreate(db);
        }

    }
}

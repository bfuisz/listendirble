package api;

import java.util.List;

import model.Category;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by benedikt.
 */
public interface CategoryApi {

    @GET("/categories/apikey/{api_key}/format/json")
    public List<Category> listCategories(@Path("api_key") String apiKey);

    @GET("/primaryCategories/apikey/{api_key}/format/json")
    public List<Category> listPrimaryCategories(@Path("api_key") String apiKey);

    @GET("/childCategories/apikey/{api_key}/primaryid/{id}/format/json")
    public List<Category> listChildCategories(@Path("api_key") String apiKey, @Path("id") long id);


}

package api;

import com.google.gson.JsonObject;

import java.util.List;


import model.RadioStation;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by benedikt.
 */
public interface StationsApi {

    @GET("/stations/apikey/{api_key}/id/{id}/format/json")
    public List<RadioStation> listCategoryStations(@Path("api_key") String apiKey, @Path("id") long id);

    @GET("/continent/apikey/{api_key}/continent/{continent_id}/format/json")
    public List<RadioStation> listContinentStations(@Path("api_key") String apiKey, @Path("continent_id") String continentId);

    @GET("/country/apikey/{api_key}/country/{country_code}/format/json")
    public List<RadioStation> listCountryStations(@Path("api_key") String apiKey, @Path("country_code") String countryCode);

    @GET("/station/apikey/{api_key}/id/{station_id}/format/json")
    public RadioStation getStation(@Path("api_key") String apiKey, @Path("station_id") long stationId);

    @GET("/amountStation/apikey/{api_key}/format/json")
    public JsonObject getStationAmount(@Path("api_key") String apiKey);

    @GET("/search/apikey/{api_key}/search/{text}")
    public void search(@Path("api_key") String apiKey, @Path("text") String text, Callback<List<RadioStation>> callback);

    @GET("/search/apikey/{api_key}/search/{text}/genre/{genre}")
    public void search(@Path("api_key") String apiKey, @Path("text") String text, @Path("genre") String genre, Callback<List<RadioStation>> callback);

}

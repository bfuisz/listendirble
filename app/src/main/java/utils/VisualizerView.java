package utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

/**
 * Created by benedikt.
 * Layout passt nut mit ImageView investigae further...
 * lineEqualizer() => http://stackoverflow.com/questions/8595692/android-visualizer-implementation-crash
 */
public class VisualizerView extends ImageView {
    private byte[] mBytes;
    private float[] mPoints;
    private Rect mRect = new Rect();
    private Paint mForePaint = new Paint();
    private int barWidth;
    private float halfHeigth;
    public boolean drawPointMatrix = true;


    public VisualizerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VisualizerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mBytes = null;
        mForePaint.setStrokeWidth(1f);
        mForePaint.setAntiAlias(true);
        mForePaint.setColor(Color.rgb(0, 128, 255));
        this.halfHeigth = this.mRect.height() / 2;
    }

    public void updateVisualizer(byte[] bytes) {
        mBytes = bytes;
        invalidate();
        requestLayout();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mBytes == null) {
            return;
        }

        if (mPoints == null || mPoints.length < mBytes.length * 4) {
            mPoints = new float[mBytes.length * 4];//fuer jeden wert der welle 4 koordinaten
        }
//          1024
//        Log.d("VisualizerVIew length", mBytes.length + "");


        mRect.set(0, 0, getWidth(), getHeight());
        this.halfHeigth = this.mRect.height() / 2;

        if (drawPointMatrix) {
//            drawPointMatrix();
            drawPointMatrix3();
            canvas.drawPoints(mPoints, mForePaint);

        } else {
            barPlusEqualizer();
            canvas.drawLines(mPoints, mForePaint);
        }
    }

    //so in die richtung
    public void drawPointMatrix() {
        if (mPoints == null || mPoints.length < mBytes.length * 2) {
            mPoints = new float[mBytes.length * 2];
        }
        int x = 10, y = 10;
        int add;
        for (int i = 0; i < this.mBytes.length - 1; i++) {
            mPoints[i * 2] = x;
            mPoints[i * 2 + 1] = y + (this.mBytes[i] + 128) * this.halfHeigth / 128;
            x += 10;
            if (x >= getWidth()) {
                x = 10;
                y += 10;
            }
        }
    }


    public void drawPointMatrix2() {
        if (mPoints == null || mPoints.length < mBytes.length * 2) {
            mPoints = new float[mBytes.length * 2];
        }
        int x = 10, y = 10;
        int add;
        for (int i = 0; i < this.mBytes.length - 1; i++) {
            mPoints[i * 2] = y + (this.mBytes[i] + 128) * this.halfHeigth / 128;
            mPoints[i * 2 + 1] = x;
            y += 10;
            if (y >= getHeight()) {
                y = 10;
                x += 10;
            }
        }
    }

    //not actually a point its a circle
    public void drawPointMatrix3() {
        if (mPoints == null || mPoints.length < mBytes.length * 2) {
            mPoints = new float[mBytes.length * 2];
        }
        float x = mRect.centerX();
        float y = mRect.centerY();
        float angleDeg = 0;
        float radius = 100f;
        int add;
        for (int i = 0; i < this.mBytes.length - 1; i++) {
            if (mBytes[i] > 0) {
                add = 1;
            } else {
                add = -1;
            }
            mPoints[i * 2] = (radius + ((byte) (this.mBytes[i] + 128)) * this.halfHeigth / 128) * (float) Math.cos(Math.toRadians(angleDeg)) + x;
            mPoints[i * 2 + 1] = (radius + ((byte) (this.mBytes[i] + 128)) * this.halfHeigth / 128) * (float) Math.sin(Math.toRadians(angleDeg)) + y;
            angleDeg += 0.77;
        }
    }

    //mit offset zwischen einzelnen segmenten und grafischen kommentaren :D
    public void segmentedEqualizer() {
        barWidth = 0;
        for (int i = 0; i < this.mBytes.length - 1; i++) {
            mPoints[i * 4] = barWidth; // x koordinate von -> -
            mPoints[i * 4 + 1] = this.halfHeigth + ((byte) (this.mBytes[i] + 128)) * this.halfHeigth / 128; // y koordinate von -> -
            barWidth += 6; //real bar width
            mPoints[i * 4 + 2] = barWidth;// x koordinate von - <-
            mPoints[i * 4 + 3] = this.halfHeigth + ((byte) (this.mBytes[i] + 128)) * this.halfHeigth / 128; // y koordinate von - <-
            barWidth += 2; //add offset to next bar
        }
    }

    public void barEqualizer() {
        //braucht mehr coordinaten
        //3 linien pro bar = 4 koordinaten pro bar
        //erste bar koordinaten paare (x/0), (x/y), (x+offset/y) (x+offset/0) wobei 0 = this.halfHeigth; is
        if (mPoints == null || mPoints.length < mBytes.length * 8) {
            mPoints = new float[mBytes.length * 8];
        }
        barWidth = 0;
        for (int i = 0; i < this.mBytes.length - 1; i++) {
            //(x/0)
            mPoints[i * 8] = barWidth;
            mPoints[i * 8 + 1] = this.halfHeigth;

            //(x/y)
            mPoints[i * 8 + 2] = barWidth;
            mPoints[i * 8 + 3] = this.halfHeigth + ((byte) (this.mBytes[i] + 128)) * this.halfHeigth / 128; //y1

            barWidth += 6;
            //(x+offset/y)
            mPoints[i * 8 + 4] = barWidth;
            mPoints[i * 8 + 5] = this.halfHeigth + ((byte) (this.mBytes[i] + 128)) * this.halfHeigth / 128; //y2

            //(x+offset/0)
            mPoints[i * 8 + 6] = barWidth;
            mPoints[i * 8 + 7] = this.halfHeigth;

            barWidth += 2;//2 offset to next bar
        }
    }

    //same as barEqualize but only in "plus" range
    public void barPlusEqualizer() {
        //braucht mehr coordinaten
        //3 linien pro bar = 4 koordinaten pro bar
        //erste bar koordinaten paare (x/0), (x/y), (x+offset/y) (x+offset/0) wobei 0 = this.halfHeigth; is
        if (mPoints == null || mPoints.length < mBytes.length * 8) {
            mPoints = new float[mBytes.length * 8];
        }
        barWidth = 0;
        Log.d("Visualizer", "{");

        for (int i = 0; i < this.mBytes.length - 1; i++) {
            Log.d("Visualizer", this.mBytes[i] + "");
            //(x/0)
            mPoints[i * 8] = barWidth;
            mPoints[i * 8 + 1] = this.halfHeigth;

            //(x/y)
            mPoints[i * 8 + 2] = barWidth;
            mPoints[i * 8 + 3] = this.halfHeigth + ((byte) (this.mBytes[i] + 128)) * this.halfHeigth / 128; //y1

            barWidth += 6;
            //(x+offset/y)
            mPoints[i * 8 + 4] = barWidth;
            mPoints[i * 8 + 5] = this.halfHeigth + ((byte) (this.mBytes[i] + 128)) * this.halfHeigth / 128; //y2

            //(x+offset/0)
            mPoints[i * 8 + 6] = barWidth;
            mPoints[i * 8 + 7] = this.halfHeigth;

            barWidth += 2;//2 offset to next bar
        }
        Log.d("Visualizer", "}");

    }

    public void lineEqualizer() {
        //wave baseline = -128 d.h. bei +128 wird praktisch auf 0 normalisiert.
        //4 punkte pro schleifendurchgang
        //x1, y1, x2, y2
        //ganze welle unter einmal
        for (int i = 0; i < this.mBytes.length - 1; i++) {
            //0 / zb 1023 = 0
            //1 / zb 1023 = 0.0009 ... mit jedem schleifendurchgang "hoeher" weil x coordinate
            //x1 lim(i -> length) d.h. i/ mBytes.length geht gegen 1 = also x1 naehrt sich mRect.width gleiches gilt fuer x2 aber um 1 nach rechts verschoben
            mPoints[i * 4] = this.mRect.width() * i / (this.mBytes.length - 1);

            // mRect.height() / 2  mitte der view
            //mByte[i] = groesser je hoeher der ton(oder so) d.h.
            mPoints[i * 4 + 1] = this.halfHeigth + ((byte) (this.mBytes[i] + 128)) * this.halfHeigth / 128; //y1
            mPoints[i * 4 + 2] = this.mRect.width() * (i + 1) / (this.mBytes.length - 1); //x2 circa lim(i -> length)
            mPoints[i * 4 + 3] = this.halfHeigth + ((byte) (this.mBytes[i + 1] + 128)) * this.halfHeigth / 128; //y2 +1 = "verbingung" fuer naechsten wert
        }
    }
}

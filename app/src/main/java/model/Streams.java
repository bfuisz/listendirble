package model;

/**
 * Created by benedikt.
 */
public class Streams {
    private long id;
    private String stream;
    private long bitrate;
    private String type;
    private int status;
    private int station;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public long getBitrate() {
        return bitrate;
    }

    public void setBitrate(long bitrate) {
        this.bitrate = bitrate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStation() {
        return station;
    }

    public void setStation(int station) {
        this.station = station;
    }

    @Override
    public String toString() {
        return "Streams{" +
                "id=" + id +
                ", stream='" + stream + '\'' +
                ", bitrate=" + bitrate +
                ", type='" + type + '\'' +
                ", status=" + status +
                ", station=" + station +
                '}';
    }
}

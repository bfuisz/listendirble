package model;


import android.content.ContentValues;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import persistency.contracts.RadioStationContract;

/**
 * Created by benedikt.
 */
public class RadioStation implements Valuable {
    private long id;
    private long categoryId;
    private String name;
    private String image;
    private String streamurl;
    private String bitrate;
    private int status;
    private String description;
    private String country;
    private String added;
    private String urlid;
    private String website;
    private boolean favorite;
    private String region;

    @SerializedName("songhistory")
    private List<SongHistoryItem> songHistory;

    @SerializedName("streams")
    private List<Streams> streams;

    @SerializedName("directory")
    private List<Directory> directories;

    public RadioStation() {
    }

    public RadioStation(long id, long categoryId, String name, String streamurl, String bitrate, int status, String description, String country, String added, String urlid, String website, List<SongHistoryItem> songHistory) {
        this.id = id;
        this.categoryId = categoryId;
        this.name = name;
        this.streamurl = streamurl;
        this.bitrate = bitrate;
        this.status = status;
        this.description = description;
        this.country = country;
        this.added = added;
        this.urlid = urlid;
        this.website = website;
        this.songHistory = songHistory;
    }


    public static RadioStation from(Cursor cursor) {
        RadioStation station = new RadioStation();
        station.id = cursor.getLong(RadioStationContract.RadioStationEntry.COLUMN_ID_INDEX);
        station.categoryId = cursor.getLong(RadioStationContract.RadioStationEntry.COLUMN_CATGEGORY_ID_INDEX);
        station.name = cursor.getString(RadioStationContract.RadioStationEntry.COLUMN_NAME_INDEX);
        station.streamurl = cursor.getString(RadioStationContract.RadioStationEntry.COLUMN_STREAM_URL_INDEX);
        station.bitrate = cursor.getString(RadioStationContract.RadioStationEntry.COLUMN_BIT_RATE_INDEX);
        station.status = cursor.getInt(RadioStationContract.RadioStationEntry.COLUMN_STATUS_INDEX);
        station.description = cursor.getString(RadioStationContract.RadioStationEntry.COLUMN_DESCRIPTION_INDEX);
        station.country = cursor.getString(RadioStationContract.RadioStationEntry.COLUMN_COUNTRY_INDEX);
        station.added = cursor.getString(RadioStationContract.RadioStationEntry.COLUMN_ADDED_INDEX);
        station.urlid = cursor.getString(RadioStationContract.RadioStationEntry.COLUMN_URL_ID_INDEX);
        station.website = cursor.getString(RadioStationContract.RadioStationEntry.COLUMN_WEBSITE_INDEX);
        station.image = cursor.getString(RadioStationContract.RadioStationEntry.COLUMN_IMAGE_INDEX);
        station.favorite = cursor.getInt(RadioStationContract.RadioStationEntry.COLUMN_FAVORITE_INDEX) == 1;
        station.region = cursor.getString(RadioStationContract.RadioStationEntry.COLUMN_REGION_INDEX);
        return station;
    }

    @Override
    public ContentValues values() {
        ContentValues values = new ContentValues();
        if (id > 0) {
            values.put(RadioStationContract.RadioStationEntry._ID, id);
        }
        values.put(RadioStationContract.RadioStationEntry.COLUMN_CATEGORY_ID, categoryId);
        values.put(RadioStationContract.RadioStationEntry.COLUMN_NAME, name);
        values.put(RadioStationContract.RadioStationEntry.COLUMN_STREAM_URL, streamurl);
        values.put(RadioStationContract.RadioStationEntry.COLUMN_BIT_RATE, bitrate);
        values.put(RadioStationContract.RadioStationEntry.COLUMN_STATUS, status);
        values.put(RadioStationContract.RadioStationEntry.COLUMN_DESCRIPTION, description);
        values.put(RadioStationContract.RadioStationEntry.COLUMN_COUNTRY, country);
        values.put(RadioStationContract.RadioStationEntry.COLUMN_ADDED, added);
        values.put(RadioStationContract.RadioStationEntry.COLUMN_URL_ID, urlid);
        values.put(RadioStationContract.RadioStationEntry.COLUMN_WEBSITE, website);
        values.put(RadioStationContract.RadioStationEntry.COLUMN_IMAGE, this.image);
        values.put(RadioStationContract.RadioStationEntry.COLUMN_FAVORITE, this.favorite);
        values.put(RadioStationContract.RadioStationEntry.COLUMN_REGION, this.region);
        return values;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreamurl() {
        return streamurl;
    }

    public void setStreamurl(String streamurl) {
        this.streamurl = streamurl;
    }

    public String getBitrate() {
        return bitrate;
    }

    public void setBitrate(String bitrate) {
        this.bitrate = bitrate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String getUrlid() {
        return urlid;
    }

    public void setUrlid(String urlid) {
        this.urlid = urlid;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public List<SongHistoryItem> getSongHistory() {
        return songHistory;
    }

    public void setSongHistory(List<SongHistoryItem> songHistory) {
        this.songHistory = songHistory;
    }

    public List<Streams> getStreams() {
        return streams;
    }

    public void setStreams(List<Streams> streams) {
        this.streams = streams;
    }

    public List<Directory> getDirectories() {
        return directories;
    }

    public void setDirectories(List<Directory> directories) {
        this.directories = directories;
    }

    public void addSongHistoryItem(SongHistoryItem shi) {
        if (this.songHistory == null) {
            this.songHistory = new ArrayList<>();
        }
        this.songHistory.add(shi);
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public boolean toggleFavorite() {
        this.favorite = !this.favorite;
        return favorite;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RadioStation)) return false;

        RadioStation station = (RadioStation) o;

        if (id != station.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "RadioStation{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", streamurl='" + streamurl + '\'' +
                ", bitrate='" + bitrate + '\'' +
                ", status=" + status +
                ", description='" + description + '\'' +
                ", country='" + country + '\'' +
                ", added='" + added + '\'' +
                ", urlid='" + urlid + '\'' +
                ", website='" + website + '\'' +
                ", songHistory=" + songHistory +
                ", streams=" + streams +
                ", directories=" + directories +
                '}';
    }
}



package model;

/**
 * Created by benedikt.
 */
public class Image {

    private String url;
    private StringHolder thumb;
    private StringHolder preview;

    public String getThumb() {
        if (thumb != null) {
            return thumb.getUrl();
        }
        return null;
    }

    public String getPreview() {
        if (preview != null) {
            return preview.getUrl();
        }
        return null;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setThumb(StringHolder thumb) {
        this.thumb = thumb;
    }

    public void setPreview(StringHolder preview) {
        this.preview = preview;
    }

    @Override
    public String toString() {
        return "Image{" +
                "url='" + url + '\'' +
                ", thumb=" + thumb +
                ", preview=" + preview +
                '}';
    }
}

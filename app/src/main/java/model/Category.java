package model;

import android.content.ContentValues;
import android.database.Cursor;

import persistency.contracts.CategoryContract;

/**
 * Created by benedikt.
 */
public class Category implements Valuable {
    private long id;
    private String name;
    private String description;
    private int amount;
    private long parent;

    public Category() {
    }

    public Category(long id, String name, String description, int amount) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.amount = amount;
    }

    public static Category from(Cursor cursor) {
        Category category = new Category();
        category.id = cursor.getLong(CategoryContract.CategoryEntry.COLUMN_ID_INDEX);
        category.name = cursor.getString(CategoryContract.CategoryEntry.COLUMN_NAME_INDEX);
        category.description = cursor.getString(CategoryContract.CategoryEntry.COLUMN_DESCRIPTION_INDEX);
        category.amount = cursor.getInt(CategoryContract.CategoryEntry.COLUMN_AMOUNT_INDEX);
        category.parent = cursor.getLong(CategoryContract.CategoryEntry.COLUMN_PARENT_INDEX);
        return category;
    }

    @Override
    public ContentValues values() {
        ContentValues values = new ContentValues();
        if (id > 0) {
            values.put(CategoryContract.CategoryEntry._ID, id);
        }
        values.put(CategoryContract.CategoryEntry.COLUMN_NAME, name);
        values.put(CategoryContract.CategoryEntry.COLUMN_DESCRIPTION, description);
        values.put(CategoryContract.CategoryEntry.COLUMN_AMOUNT, amount);
        values.put(CategoryContract.CategoryEntry.COLUMN_PARENT, parent);

        return values;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public long getParent() {
        return parent;
    }

    public void setParent(long parent) {
        this.parent = parent;
    }
}

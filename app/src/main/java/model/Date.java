package model;

import android.util.Log;

import java.text.SimpleDateFormat;

/**
 * Created by benedikt.
 */
public class Date {
    private SimpleDateFormat formattedDate = new SimpleDateFormat("dd MM yyyy HH:mm:ss");
    private long sec;
    private long usec;

    public long getSec() {
        return sec;
    }

    public void setSec(long sec) {
        this.sec = sec;
    }

    public long getUsec() {
        return usec;
    }

    public void setUsec(long usec) {
        this.usec = usec;
    }

    private String getUsecString(long seconds) {
        long hours = seconds / 3600;
        long minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;
        return String.format("%d %d %d", hours, minutes, seconds);
    }

    @Override
    public String toString() {
        return formattedDate.format(new java.util.Date(sec * 1000)) + " " + getUsecString(usec);
    }

}

package model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

/**
 * Created by benedikt.
 */
public class Info {
    private JsonObject _id;
    private Image image;
    private String name;
    private String title;
    private StringHolder urls;
    private String thumbnailUrl;


    public JsonObject get_id() {
        return _id;
    }

    public void set_id(JsonObject _id) {
        this._id = _id;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public StringHolder getUrls() {
        return urls;
    }

    public void setUrls(StringHolder urls) {
        this.urls = urls;
    }


    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    @Override
    public String toString() {
        return "Info{" +
                "_id=" + _id +
                ", image=" + image +
                ", name='" + name + '\'' +
                ", title='" + title + '\'' +
                ", urls=" + urls +
                ", thumbnailUrl='" + thumbnailUrl + '\'' +
                '}';
    }

    public String getThumbnailUrl() {
        if (image != null) {
            return image.getThumb();
        }
        return null;
    }

    public String getImageUrl() {
        if (image != null) {
            return image.getUrl();
        }
        return null;
    }
}

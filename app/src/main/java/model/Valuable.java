package model;

import android.content.ContentValues;

/**
 * Created by benedikt.
 */
public interface Valuable {
    public ContentValues values();
}

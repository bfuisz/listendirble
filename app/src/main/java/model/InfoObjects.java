package model;

import com.google.gson.JsonObject;

/**
 * Created by benedikt.
 */
public class InfoObjects {
    private JsonObject _id;
    private Image image;
    private String name;
    private String title;
    private String urls;


    public JsonObject get_id() {
        return _id;
    }

    public void set_id(JsonObject _id) {
        this._id = _id;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrls() {
        return urls;
    }

    public void setUrls(String urls) {
        this.urls = urls;
    }

    @Override
    public String toString() {
        return "Info{" +
                "_id=" + _id +
                ", image=" + image +
                ", name='" + name + '\'' +
                ", title='" + title + '\'' +
                ", urls='" + urls + '\'' +
                '}';
    }
}

package model;

import com.google.gson.JsonObject;

/**
 * Created by benedikt.
 */
public class SongHistoryItem {
    private JsonObject _id;
    private String name;
    private String title;
    private String time;
    private Info info;
    private Date date;

    public SongHistoryItem() {
    }

    public SongHistoryItem(JsonObject _id, String name, String title, String time) {
        this._id = _id;
        this.name = name;
        this.title = title;
        this.time = time;
    }

    public JsonObject getId() {
        return _id;
    }

    public void setId(JsonObject _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }


    public String getThumbnail() {
        if (info != null) {
            return info.getThumbnailUrl();
        }
        return null;
    }

    public String getImage() {
        if (info != null) {
            return info.getImageUrl();
        }
        return null;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SongHistoryItem)) return false;

        SongHistoryItem that = (SongHistoryItem) o;

        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SongHistoryItem{" +
                "_id=" + _id +
                ", name='" + name + '\'' +
                ", title='" + title + '\'' +
                ", time='" + time + '\'' +
                ", info=" + info +
                ", date=" + date +
                '}' + "\n";
    }
}

package model;

/**
 * Created by benedikt.
 */
public class StringHolder {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "StringHolder{" +
                "url='" + url + '\'' +
                '}';
    }
}

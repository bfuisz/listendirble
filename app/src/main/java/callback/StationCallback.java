package callback;

import android.provider.MediaStore;

import model.RadioStation;

/**
 * Created by benedikt.
 */
public interface StationCallback {
    public void stationLoaded(RadioStation station);
}
